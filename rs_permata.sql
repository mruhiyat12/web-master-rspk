-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 03, 2017 at 02:49 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rs_permata`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `article_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `content` text,
  `img` varchar(255) DEFAULT NULL,
  `article_category_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_datetime` datetime DEFAULT NULL,
  `is_publish` tinyint(1) DEFAULT '1',
  `counter` int(11) DEFAULT '1',
  `id_rumahsakit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `article_category`
--

CREATE TABLE `article_category` (
  `article_category_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `description` text,
  `img` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_removeable` tinyint(1) DEFAULT '1',
  `is_video` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coment`
--

CREATE TABLE `coment` (
  `coment_id` int(11) NOT NULL,
  `article_id` int(11) DEFAULT NULL,
  `parent_coment_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `content` text,
  `is_approve` tinyint(1) DEFAULT '0',
  `created_datetime` datetime DEFAULT NULL,
  `img` varchar(255) DEFAULT 'avatar.png'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dokter`
--

CREATE TABLE `dokter` (
  `id_dokter` int(11) NOT NULL,
  `nama_dokter` varchar(255) DEFAULT NULL,
  `img` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `bagian` varchar(150) DEFAULT NULL,
  `id_rumahsakit` int(11) NOT NULL,
  `ket` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `download_history`
--

CREATE TABLE `download_history` (
  `download_id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `ket` text,
  `ebook_id` int(11) DEFAULT NULL,
  `download_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebook`
--

CREATE TABLE `ebook` (
  `ebook_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `ket` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `ebook_category_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_datetime` datetime NOT NULL,
  `is_publish` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ebook_category`
--

CREATE TABLE `ebook_category` (
  `ebook_category_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `description` text,
  `img` varchar(255) DEFAULT 'default-img.png',
  `is_active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `gallery_id` int(11) NOT NULL,
  `gallery_name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_datetime` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `is_publish` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_detail`
--

CREATE TABLE `gallery_detail` (
  `gallery_detail_id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `description` text,
  `is_active` tinyint(1) DEFAULT '1',
  `alt` varchar(255) DEFAULT NULL,
  `is_cover` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `guestbook`
--

CREATE TABLE `guestbook` (
  `guestbook_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `is_processed` tinyint(1) DEFAULT '0',
  `company` varchar(255) DEFAULT NULL,
  `address` text,
  `type` enum('guestbook','portfolio','lamaran','') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_dokter`
--

CREATE TABLE `jadwal_dokter` (
  `id_jadwal` int(11) NOT NULL,
  `hari` varchar(10) NOT NULL,
  `jam` time NOT NULL,
  `id_dokter` varchar(255) DEFAULT NULL,
  `spesialis` varchar(255) DEFAULT NULL,
  `id_rumahsakit` int(11) NOT NULL,
  `ket` varchar(200) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_user`
--

CREATE TABLE `m_user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(64) DEFAULT NULL,
  `password` varchar(100) DEFAULT '5600e318a8eb1b54a5dcb60c3a793eaa892f34f2b0d5c3e6145bbdd979907e2f',
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `level` enum('admin','user') DEFAULT 'user',
  `is_active` tinyint(1) DEFAULT '1',
  `img` varchar(255) DEFAULT 'default-user.png',
  `position` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `content` text,
  `img` varchar(255) DEFAULT 'default-page.png',
  `is_publish` tinyint(1) DEFAULT '1',
  `created_datetime` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_datetime` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `counter` int(11) DEFAULT '1',
  `is_removeable` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rumah_sakit`
--

CREATE TABLE `rumah_sakit` (
  `id_rumahsakit` int(11) NOT NULL,
  `nama_rumahsakit` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `setting_id` int(11) NOT NULL,
  `name_set` varchar(255) DEFAULT NULL,
  `value_set` text,
  `description` text,
  `is_active` tinyint(1) DEFAULT '1',
  `is_system` tinyint(1) DEFAULT '0',
  `is_removeable` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `slide`
--

CREATE TABLE `slide` (
  `slide_id` int(11) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `alt` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_article`
-- (See below for the actual view)
--
CREATE TABLE `v_article` (
`article_id` int(11)
,`title` varchar(255)
,`link` varchar(255)
,`content` text
,`img` varchar(255)
,`article_category_id` int(11)
,`category_name` varchar(255)
,`created_by` int(11)
,`username` varchar(64)
,`name` varchar(255)
,`created_datetime` datetime
,`updated_by` int(11)
,`updated_datetime` datetime
,`is_publish` tinyint(1)
,`counter` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_comment`
-- (See below for the actual view)
--
CREATE TABLE `v_comment` (
`link` varchar(255)
,`article_content` text
,`coment_id` int(11)
,`article_id` int(11)
,`parent_coment_id` int(11)
,`name` varchar(255)
,`email` varchar(255)
,`content` text
,`is_approve` tinyint(1)
,`created_datetime` datetime
,`img` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_dokter`
-- (See below for the actual view)
--
CREATE TABLE `v_dokter` (
`id_dokter` int(11)
,`nama_dokter` varchar(255)
,`img` varchar(255)
,`alamat` varchar(255)
,`bagian` varchar(150)
,`id_rumahsakit` int(11)
,`ket` varchar(255)
,`is_active` tinyint(1)
,`nama_rumahsakit` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_download`
-- (See below for the actual view)
--
CREATE TABLE `v_download` (
`download_id` int(11)
,`name` varchar(200)
,`email` varchar(255)
,`ket` text
,`ebook_id` int(11)
,`download_datetime` datetime
,`ebook_title` varchar(255)
,`ebook_ket` text
,`category_ebook_name` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_ebook`
-- (See below for the actual view)
--
CREATE TABLE `v_ebook` (
`ebook_id` int(11)
,`title` varchar(255)
,`link` varchar(255)
,`ket` text
,`img` varchar(255)
,`ebook_category_id` int(11)
,`created_by` int(11)
,`created_datetime` datetime
,`updated_by` int(11)
,`updated_datetime` datetime
,`is_publish` tinyint(1)
,`category_name` varchar(255)
,`name_create` varchar(255)
,`name_update` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_gallery`
-- (See below for the actual view)
--
CREATE TABLE `v_gallery` (
`gallery_id` int(11)
,`gallery_name` varchar(255)
,`created_datetime` datetime
,`created_by` int(11)
,`link` varchar(255)
,`is_publish` tinyint(1)
,`gallery_detail_id` int(11)
,`title` varchar(255)
,`img` varchar(255)
,`description` text
,`is_active` tinyint(1)
,`alt` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_gallery_header`
-- (See below for the actual view)
--
CREATE TABLE `v_gallery_header` (
`gallery_id` int(11)
,`gallery_name` varchar(255)
,`description` text
,`created_datetime` datetime
,`created_by` int(11)
,`link` varchar(255)
,`is_publish` tinyint(1)
,`cover` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_jadwaldokter`
-- (See below for the actual view)
--
CREATE TABLE `v_jadwaldokter` (
`id_jadwal` int(11)
,`hari` varchar(10)
,`jam` time
,`id_dokter` varchar(255)
,`spesialis` varchar(255)
,`id_rumahsakit` int(11)
,`ket` varchar(200)
,`is_active` tinyint(1)
,`nama_dokter` varchar(255)
,`nama_rumahsakit` varchar(255)
);

-- --------------------------------------------------------

--
-- Structure for view `v_article`
--
DROP TABLE IF EXISTS `v_article`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_article`  AS  select `a`.`article_id` AS `article_id`,`a`.`title` AS `title`,`a`.`link` AS `link`,`a`.`content` AS `content`,`a`.`img` AS `img`,`a`.`article_category_id` AS `article_category_id`,`c`.`category_name` AS `category_name`,`a`.`created_by` AS `created_by`,`u`.`username` AS `username`,`u`.`name` AS `name`,`a`.`created_datetime` AS `created_datetime`,`a`.`updated_by` AS `updated_by`,`a`.`updated_datetime` AS `updated_datetime`,`a`.`is_publish` AS `is_publish`,`a`.`counter` AS `counter` from ((`article` `a` left join `article_category` `c` on((`a`.`article_category_id` = `c`.`article_category_id`))) left join `m_user` `u` on((`a`.`created_by` = `u`.`user_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_comment`
--
DROP TABLE IF EXISTS `v_comment`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_comment`  AS  select `a`.`link` AS `link`,`a`.`content` AS `article_content`,`c`.`coment_id` AS `coment_id`,`c`.`article_id` AS `article_id`,`c`.`parent_coment_id` AS `parent_coment_id`,`c`.`name` AS `name`,`c`.`email` AS `email`,`c`.`content` AS `content`,`c`.`is_approve` AS `is_approve`,`c`.`created_datetime` AS `created_datetime`,`c`.`img` AS `img` from (`coment` `c` join `article` `a` on((`c`.`article_id` = `a`.`article_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_dokter`
--
DROP TABLE IF EXISTS `v_dokter`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_dokter`  AS  select `dok`.`id_dokter` AS `id_dokter`,`dok`.`nama_dokter` AS `nama_dokter`,`dok`.`img` AS `img`,`dok`.`alamat` AS `alamat`,`dok`.`bagian` AS `bagian`,`dok`.`id_rumahsakit` AS `id_rumahsakit`,`dok`.`ket` AS `ket`,`dok`.`is_active` AS `is_active`,`rs`.`nama_rumahsakit` AS `nama_rumahsakit` from (`dokter` `dok` left join `rumah_sakit` `rs` on((`dok`.`id_rumahsakit` = `rs`.`id_rumahsakit`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_download`
--
DROP TABLE IF EXISTS `v_download`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_download`  AS  select `d`.`download_id` AS `download_id`,`d`.`name` AS `name`,`d`.`email` AS `email`,`d`.`ket` AS `ket`,`d`.`ebook_id` AS `ebook_id`,`d`.`download_datetime` AS `download_datetime`,`e`.`title` AS `ebook_title`,`e`.`ket` AS `ebook_ket`,`e`.`category_name` AS `category_ebook_name` from (`download_history` `d` join `v_ebook` `e` on((`e`.`ebook_id` = `d`.`ebook_id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_ebook`
--
DROP TABLE IF EXISTS `v_ebook`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_ebook`  AS  select `e`.`ebook_id` AS `ebook_id`,`e`.`title` AS `title`,`e`.`link` AS `link`,`e`.`ket` AS `ket`,`e`.`img` AS `img`,`e`.`ebook_category_id` AS `ebook_category_id`,`e`.`created_by` AS `created_by`,`e`.`created_datetime` AS `created_datetime`,`e`.`updated_by` AS `updated_by`,`e`.`updated_datetime` AS `updated_datetime`,`e`.`is_publish` AS `is_publish`,`c`.`category_name` AS `category_name`,`u`.`name` AS `name_create`,(select `mu`.`name` from `m_user` `mu` where (`mu`.`user_id` = `e`.`updated_by`)) AS `name_update` from ((`ebook` `e` join `ebook_category` `c` on((`e`.`ebook_category_id` = `c`.`ebook_category_id`))) join `m_user` `u` on((`u`.`user_id` = `e`.`created_by`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_gallery`
--
DROP TABLE IF EXISTS `v_gallery`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_gallery`  AS  select `h`.`gallery_id` AS `gallery_id`,`h`.`gallery_name` AS `gallery_name`,`h`.`created_datetime` AS `created_datetime`,`h`.`created_by` AS `created_by`,`h`.`link` AS `link`,`h`.`is_publish` AS `is_publish`,`d`.`gallery_detail_id` AS `gallery_detail_id`,`d`.`title` AS `title`,`d`.`img` AS `img`,`d`.`description` AS `description`,`d`.`is_active` AS `is_active`,`d`.`alt` AS `alt` from (`gallery` `h` join `gallery_detail` `d`) where (`h`.`gallery_id` = `d`.`gallery_id`) ;

-- --------------------------------------------------------

--
-- Structure for view `v_gallery_header`
--
DROP TABLE IF EXISTS `v_gallery_header`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_gallery_header`  AS  select `g`.`gallery_id` AS `gallery_id`,`g`.`gallery_name` AS `gallery_name`,`g`.`description` AS `description`,`g`.`created_datetime` AS `created_datetime`,`g`.`created_by` AS `created_by`,`g`.`link` AS `link`,`g`.`is_publish` AS `is_publish`,(case when isnull((select `d`.`img` from `gallery_detail` `d` where ((`d`.`gallery_id` = `g`.`gallery_id`) and (`d`.`is_cover` = 1) and (`d`.`is_active` = 1)) limit 1)) then 'default-image.png' else (select `e`.`img` from `gallery_detail` `e` where ((`e`.`gallery_id` = `g`.`gallery_id`) and (`e`.`is_cover` = 1) and (`e`.`is_active` = 1)) limit 1) end) AS `cover` from `gallery` `g` ;

-- --------------------------------------------------------

--
-- Structure for view `v_jadwaldokter`
--
DROP TABLE IF EXISTS `v_jadwaldokter`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_jadwaldokter`  AS  select `j`.`id_jadwal` AS `id_jadwal`,`j`.`hari` AS `hari`,`j`.`jam` AS `jam`,`j`.`id_dokter` AS `id_dokter`,`j`.`spesialis` AS `spesialis`,`j`.`id_rumahsakit` AS `id_rumahsakit`,`j`.`ket` AS `ket`,`j`.`is_active` AS `is_active`,`d`.`nama_dokter` AS `nama_dokter`,`r`.`nama_rumahsakit` AS `nama_rumahsakit` from ((`jadwal_dokter` `j` join `dokter` `d` on((`j`.`id_dokter` = `d`.`id_dokter`))) join `rumah_sakit` `r` on((`j`.`id_rumahsakit` = `r`.`id_rumahsakit`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`article_id`),
  ADD UNIQUE KEY `link` (`link`),
  ADD KEY `article_category_id` (`article_category_id`,`created_by`,`updated_by`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `id_rumahsakit` (`id_rumahsakit`);

--
-- Indexes for table `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`article_category_id`),
  ADD UNIQUE KEY `link` (`link`);

--
-- Indexes for table `coment`
--
ALTER TABLE `coment`
  ADD PRIMARY KEY (`coment_id`),
  ADD KEY `article_id` (`article_id`,`parent_coment_id`),
  ADD KEY `parent_coment_id` (`parent_coment_id`);

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id_dokter`),
  ADD KEY `id_rumahsakit` (`id_rumahsakit`);

--
-- Indexes for table `download_history`
--
ALTER TABLE `download_history`
  ADD PRIMARY KEY (`download_id`),
  ADD KEY `ebook_id` (`ebook_id`);

--
-- Indexes for table `ebook`
--
ALTER TABLE `ebook`
  ADD PRIMARY KEY (`ebook_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `update_by` (`updated_by`),
  ADD KEY `ebook_category_id` (`ebook_category_id`);

--
-- Indexes for table `ebook_category`
--
ALTER TABLE `ebook_category`
  ADD PRIMARY KEY (`ebook_category_id`),
  ADD KEY `link` (`link`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`gallery_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `gallery_detail`
--
ALTER TABLE `gallery_detail`
  ADD PRIMARY KEY (`gallery_detail_id`),
  ADD KEY `gallery_id` (`gallery_id`);

--
-- Indexes for table `guestbook`
--
ALTER TABLE `guestbook`
  ADD PRIMARY KEY (`guestbook_id`);

--
-- Indexes for table `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`),
  ADD UNIQUE KEY `link` (`link`),
  ADD KEY `created_by` (`created_by`,`updated_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `rumah_sakit`
--
ALTER TABLE `rumah_sakit`
  ADD PRIMARY KEY (`id_rumahsakit`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`setting_id`),
  ADD UNIQUE KEY `name_set` (`name_set`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`slide_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `article_category`
--
ALTER TABLE `article_category`
  MODIFY `article_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `coment`
--
ALTER TABLE `coment`
  MODIFY `coment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `download_history`
--
ALTER TABLE `download_history`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `ebook`
--
ALTER TABLE `ebook`
  MODIFY `ebook_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ebook_category`
--
ALTER TABLE `ebook_category`
  MODIFY `ebook_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `gallery_detail`
--
ALTER TABLE `gallery_detail`
  MODIFY `gallery_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `guestbook`
--
ALTER TABLE `guestbook`
  MODIFY `guestbook_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `rumah_sakit`
--
ALTER TABLE `rumah_sakit`
  MODIFY `id_rumahsakit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `slide_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`article_category_id`) REFERENCES `article_category` (`article_category_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `article_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `m_user` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `article_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `m_user` (`user_id`) ON UPDATE CASCADE;

--
-- Constraints for table `coment`
--
ALTER TABLE `coment`
  ADD CONSTRAINT `coment_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `coment_ibfk_2` FOREIGN KEY (`parent_coment_id`) REFERENCES `coment` (`coment_id`) ON UPDATE CASCADE;

--
-- Constraints for table `dokter`
--
ALTER TABLE `dokter`
  ADD CONSTRAINT `dokter_ibfk_1` FOREIGN KEY (`id_rumahsakit`) REFERENCES `rumah_sakit` (`id_rumahsakit`);

--
-- Constraints for table `download_history`
--
ALTER TABLE `download_history`
  ADD CONSTRAINT `download_history_ibfk_1` FOREIGN KEY (`ebook_id`) REFERENCES `ebook` (`ebook_id`) ON UPDATE CASCADE;

--
-- Constraints for table `ebook`
--
ALTER TABLE `ebook`
  ADD CONSTRAINT `ebook_ibfk_1` FOREIGN KEY (`ebook_category_id`) REFERENCES `ebook_category` (`ebook_category_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ebook_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `m_user` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ebook_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `m_user` (`user_id`) ON UPDATE CASCADE;

--
-- Constraints for table `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `page_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `m_user` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `page_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `m_user` (`user_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
