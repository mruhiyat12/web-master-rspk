-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 08 Okt 2017 pada 05.14
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms_sbc`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `article`
--

CREATE TABLE `article` (
  `article_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `content` text,
  `img` varchar(255) DEFAULT NULL,
  `article_category_id` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_datetime` datetime DEFAULT NULL,
  `is_publish` tinyint(1) DEFAULT '1',
  `counter` int(11) DEFAULT '1',
  `id_rumahsakit` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `article`
--

INSERT INTO `article` (`article_id`, `title`, `link`, `content`, `img`, `article_category_id`, `created_by`, `created_datetime`, `updated_by`, `updated_datetime`, `is_publish`, `counter`, `id_rumahsakit`) VALUES
(26, 'Fun English Training', 'ss', '<p style=\"text-align:justify\">Fun English Training (FET) merupakan program yang bertujuan untuk melatih berbicara bahasa Inggris secara alami dan percaya diri. Materi yang diberikan dalam pelatihan ini diantaranya Enriching Vocabulary, Pronunciation, English Speaking, Grammar. Media pelatihan ini menggunakan audio visual (audio dan video bahasa Inggris). Agar dapat mempermudah peserta dalam menguasai keterampilan bahasa Inggris secara efektif, metode yang digunakan yaitu <em>fun learning activity.</em></p>\r\n\r\n<p>&nbsp;</p>', 'slide21.jpg', 1, 2, '2017-06-06 08:38:24', 2, '2017-06-10 10:49:15', 1, 1, 0),
(28, 'Motivation', 'program-2', '<p><strong><span style=\"font-size:12.0pt\">Motivation </span></strong></p>\r\n\r\n<p>Pelatihan <em>Motivation </em>merupakan program untuk memotivasi seseorang agar memiliki kebiasaan yang baik, pola pikir dinamis, menjadi teladan, mampu beradaptasi dengan cepat, menumbuhkan empati, menghormati dan menghargai sesama, dan pandai berkomunikasi baik secara intrapersonal maupun interpersonal. Setelah mengikuti pelatihan ini, peserta memiliki tingkat kepercayaan yang lebih tinggi, motivasi kuat dalam mewujudkan harapan, manajemen emosi yang baik, dan mampu mengoptimalkan potensi diri.</p>', 'free.png', 1, 2, '2017-06-06 09:39:19', 2, '2017-06-10 10:48:14', 1, 1, 0),
(29, 'Public Speaking', 'program-3', '<p><strong>Pelatihan <em>Public Speaking</em> ini dirancang untuk meningkatkan kualitas dan efektivitas berbicara di depan umum. Selain itu juga&nbsp; peserta dapat&nbsp; mengatasi rasa gugup, meningkatkan keberanian dan kepercayaan diri, berpikir kreatif, menyampaikan informasi secara jelas, meningkatkan prestasi, lebih berkharisma,mengotimalkan kekuatan 3 V (visual, voice, dan verbal), menguasai teknik <em>gesture</em>,&nbsp; dapat menguasai audiens yang berbeda-beda, mempengaruhi orang lain, dan menjadi <em>public speaker</em> yang menyegarkan.</strong></p>', 'responsive.png', 1, 2, '2017-06-06 09:46:51', 2, '2017-06-10 10:47:11', 1, 1, 0),
(30, 'Promo 1', 'promo-1', '<p><u><strong>qqqqq</strong></u></p>\r\n\r\n<p><u><strong>qqqqq</strong></u></p>\r\n\r\n<p><u><strong>qqqqq</strong></u></p>\r\n\r\n<p>Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1 Pormo 1 Promo 1</p>', 'ukas-quality-management.png', 2, 2, '2017-06-06 10:05:27', 2, '2017-06-06 10:30:53', 1, 1, 0),
(31, 'Promo 2', 'promo-2', '<p>Promo 2 Promo 2</p>\r\n\r\n<p>Promo 2 Promo 2</p>\r\n\r\n<p>Promo 2 Promo 2</p>\r\n\r\n<p>Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2 Promo 2</p>', 'slide3.jpg', 2, 2, '2017-06-06 10:31:43', 2, '2017-06-06 10:31:55', 1, 1, 0),
(32, 'PROMO 3', 'promo-3', '<p>PROMO 3 PROMO 3</p>\r\n\r\n<p>PROMO 3 PROMO 3</p>\r\n\r\n<p>PROMO 3 PROMO 3</p>\r\n\r\n<p>PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3 PROMO 3</p>', '2017-07-05_133334.jpg', 2, 2, '2017-06-06 10:32:24', 2, '2017-06-06 10:32:30', 1, 1, 0),
(33, 'artikel 1', 'artikel-1', '<p>artikel 1artikel 1artikel 1artikel 1artikel 1artikel 1artikel 1artikel 1artikel 1artikel 1artikel 1artikel 1</p>\r\n\r\n<p>artikel 1artikel 1artikel 1artikel 1artikel 1artikel 1</p>\r\n\r\n<p>artikel 1artikel 1artikel 1artikel 1artikel 1artikel 1artikel 1artikel 1</p>\r\n\r\n<p>artikel 1artikel 1artikel 1artikel 1artikel 1</p>\r\n\r\n<p>artikel 1artikel 1artikel 1artikel 1artikel 1artikel 1</p>', NULL, 3, 2, '2017-06-06 10:45:36', 2, '2017-06-06 10:45:47', 1, 1, 0),
(34, 'artikel 2', 'artikel-2', '<p>artikel 2artikel 2artikel 2artikel 2artikel 2artikel 2artikel 2artikel 2artikel 2</p>\r\n\r\n<p>artikel 2artikel 2artikel 2artikel 2artikel 2</p>\r\n\r\n<p>artikel 2artikel 2artikel 2artikel 2artikel 2artikel 2</p>\r\n\r\n<p>artikel 2artikel 2artikel 2artikel 2artikel 2</p>\r\n\r\n<p>artikel 2artikel 2artikel 2artikel 2artikel 2</p>', NULL, 3, 2, '2017-06-06 10:46:23', 2, '2017-06-06 10:46:27', 1, 1, 0),
(35, 'artikel 3', 'artikel-3', '<p>artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3</p>\r\n\r\n<p>artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3</p>\r\n\r\n<p>artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3</p>\r\n\r\n<p>artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3</p>\r\n\r\n<p>artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3 artikel 3</p>\r\n\r\n<p>&nbsp;</p>', NULL, 3, 2, '2017-06-06 10:47:18', 2, '2017-06-06 10:47:23', 1, 1, 0),
(36, 'artikel 4', 'artikel-4', '<p>artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4</p>\r\n\r\n<p>artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4</p>\r\n\r\n<p>artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4</p>\r\n\r\n<p>artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4</p>\r\n\r\n<p>artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4 artikel 4</p>', NULL, 3, 2, '2017-06-06 10:48:05', 2, '2017-06-06 10:48:05', 1, 1, 0),
(37, 'VIDEO 1', 'video-1', 'http://www.youtube.com/embed/XGSy3_Czz8k', NULL, 4, 2, '2017-06-07 11:00:58', 2, '2017-06-07 11:00:58', 1, 1, 0),
(38, 'Video 2', 'video-2', 'https://www.youtube.com/embed/x9q14hNtq8w', NULL, 4, 2, '2017-06-07 12:26:56', 2, '2017-06-07 12:27:04', 1, 1, 0),
(39, 'video 3', 'video-3', 'https://www.youtube.com/embed/sAYB8DTvl7Q?ecver=1', NULL, 4, 2, '2017-06-07 12:28:10', 2, '2017-06-07 12:30:40', 1, 1, 0),
(40, 'VIDEO 4', 'video-4', 'https://www.youtube.com/embed/7O7XCt8K5tc?ecver=1', NULL, 4, 2, '2017-06-07 12:31:42', 2, '2017-06-07 12:31:52', 1, 1, 0),
(47, 'artikel 5', 'artikel-5', '<p>artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5</p>\r\n\r\n<p>artikel 5artikel 5artikel 5artikel 5artikel 5artikel 5</p>\r\n\r\n<h2 style=\"font-style:italic;\">artikel 5artikel 5artikel 5artikel 5artikel 5</h2>', NULL, 3, 2, '2017-07-11 20:50:29', 2, '2017-07-11 20:50:29', 1, 1, 0),
(48, 'artikel 6', 'artikel-6', '<div style=\"background:#eeeeee;border:1px solid #cccccc;padding:5px 10px;\">artikel 6artikel 6artikel 6artikel 6artikel 6</div>\r\n\r\n<p>artikel 6artikel 6artikel 6artikel 6</p>\r\n\r\n<p>artikel 6artikel 6artikel 6artikel 6</p>', 'foto-foto-pemandangan-padang-rumput-9.jpg', 3, 2, '2017-07-11 20:52:09', 2, '2017-07-11 20:52:32', 1, 1, 0),
(49, 'artikel 7', 'artikel-7', '<p>artikel 7artikel 7artikel 7artikel 7</p>\r\n\r\n<p>artikel 7artikel 7artikel 7artikel 7artikel 7</p>\r\n\r\n<p>artikel 7artikel 7artikel 7artikel 7artikel 7</p>', '91-iso9001-rgb-120.jpg', 3, 2, '2017-07-11 20:52:55', 2, '2017-07-11 20:53:11', 1, 1, 0),
(50, 'Promo 4', 'promo-4', '<p>Promo 4Promo 4Promo 4Promo 4</p>\r\n\r\n<p>Promo 4Promo 4Promo 4Promo 4</p>\r\n\r\n<p>Promo 4Promo 4Promo 4</p>', '0f41cf9a5b.jpg', 2, 2, '2017-07-11 21:26:29', 2, '2017-07-11 21:26:29', 1, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `article_category`
--

CREATE TABLE `article_category` (
  `article_category_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `description` text,
  `img` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `is_removeable` tinyint(1) DEFAULT '1',
  `is_video` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `article_category`
--

INSERT INTO `article_category` (`article_category_id`, `category_name`, `link`, `description`, `img`, `is_active`, `is_removeable`, `is_video`) VALUES
(1, 'Informasi', 'program', '<p>Informasi</p>', NULL, 1, 1, 0),
(2, 'jadwal dokter', 'promo', '<p>jadwal dokter</p>', NULL, 1, 1, 0),
(3, 'lowongan kerja', 'artikel', '<p>lowongan kerja</p>', NULL, 1, 1, 0),
(4, 'Video', 'video', '<p>VIDEO</p>', NULL, 1, 0, 1),
(5, 'Dokter Team', 'dokter-team', '<p>Dokter Team</p>', NULL, 1, 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `coment`
--

CREATE TABLE `coment` (
  `coment_id` int(11) NOT NULL,
  `article_id` int(11) DEFAULT NULL,
  `parent_coment_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `content` text,
  `is_approve` tinyint(1) DEFAULT '0',
  `created_datetime` datetime DEFAULT NULL,
  `img` varchar(255) DEFAULT 'avatar.png'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `coment`
--

INSERT INTO `coment` (`coment_id`, `article_id`, `parent_coment_id`, `name`, `email`, `content`, `is_approve`, `created_datetime`, `img`) VALUES
(1, 26, 1, 'wachid', 'wachid@mail.com', 'testing komentar', 1, '2017-08-13 00:00:00', 'avatar.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dokter`
--

CREATE TABLE `dokter` (
  `id_dokter` int(11) NOT NULL,
  `nama_dokter` varchar(255) DEFAULT NULL,
  `img` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `bagian` varchar(150) DEFAULT NULL,
  `id_rumahsakit` int(11) NOT NULL,
  `ket` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dokter`
--

INSERT INTO `dokter` (`id_dokter`, `nama_dokter`, `img`, `alamat`, `bagian`, `id_rumahsakit`, `ket`, `is_active`) VALUES
(0, 'Dr.Harun', '', 'Karawang', 'Spesialis Umum', 1, '<p>Dddddd</p>\r\n', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `download_history`
--

CREATE TABLE `download_history` (
  `download_id` int(11) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `ket` text,
  `ebook_id` int(11) DEFAULT NULL,
  `download_datetime` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `download_history`
--

INSERT INTO `download_history` (`download_id`, `name`, `email`, `ket`, `ebook_id`, `download_datetime`) VALUES
(1, 'hh', 'FFGF@MAIL.COM', 'mj', NULL, '2017-07-06 22:42:48'),
(2, 'dhsh', 'sssa@kdsj.com', 'dkjhwudhuwhd', NULL, '2017-07-06 22:49:24'),
(3, 'csc', 'AA@MAIL.COM', 'dsds', NULL, '2017-07-09 15:05:46'),
(4, 'asas', 'AA@MAIL.COM', 'ddvd', NULL, '2017-07-09 15:06:47'),
(5, 'dsd', 'AA@MAIL.COM', 'vcvdcv', NULL, '2017-07-09 15:09:48'),
(6, 'dsd', 'FFGF@MAIL.COM', 'dfdf', NULL, '2017-07-09 15:10:45'),
(7, 'rr', 'FFGF@MAIL.COM', 'fefefe', NULL, '2017-07-09 15:10:59'),
(8, 'sd', 'AA@MAIL.COM', 'dfwsds', NULL, '2017-07-09 15:11:37'),
(9, 'Agus', 'agus@gmail.com', 'test', NULL, '2017-07-09 15:23:10'),
(10, 'test', 'test@gmail.com', 'test', NULL, '2017-07-09 15:24:26'),
(11, 'dfwe', 'FFGF@MAIL.COM', 'feddefe', NULL, '2017-07-09 15:49:27'),
(12, 'e', 'AA@MAIL.COM', 'efe', NULL, '2017-07-09 15:50:20'),
(21, 'wachid', 'wachidrudyanto@gmail.com', 'alangkah baiknya jika', 8, '2017-07-10 16:02:18'),
(22, 'saya', 'saya@mail.com', 'fffff', 8, '2017-07-19 21:13:37');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ebook`
--

CREATE TABLE `ebook` (
  `ebook_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `ket` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `ebook_category_id` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_datetime` datetime NOT NULL,
  `is_publish` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ebook`
--

INSERT INTO `ebook` (`ebook_id`, `title`, `link`, `ket`, `img`, `ebook_category_id`, `created_by`, `created_datetime`, `updated_by`, `updated_datetime`, `is_publish`) VALUES
(3, 'Panduan Sholat 1', 'www.abcdef.com', '<p>Panduan Sholat 1</p>', '', 3, 2, '2017-07-10 15:57:23', 2, '2017-07-31 09:16:18', 1),
(4, 'Panduan Sholat 2', 'www.qwerty.com', '<p>testing 2</p>', '', 3, 2, '2017-07-10 15:58:00', 2, '2017-07-10 15:58:00', 1),
(5, 'Rahasia Sukses 1', 'www.coba.com', '<p>testing 3</p>', '', 2, 2, '2017-07-10 15:58:33', 2, '2017-07-10 15:59:13', 1),
(6, 'Rahasia Sukses 2', 'www.coba2.com', '<p>testing 4</p>', '', 2, 2, '2017-07-10 15:59:42', 2, '2017-07-10 15:59:42', 1),
(7, 'Kehidupan sosial 1', 'www.ddd.com', '<p>Kehidupan sosial 1</p>', 'slide3.jpg', 4, 2, '2017-07-10 16:00:37', 2, '2017-07-31 09:13:27', 1),
(8, 'Kehidupan sosial 2', 'www.klmn.com', '<p>testing 6</p>', '', 4, 2, '2017-07-10 16:01:06', 2, '2017-07-10 16:01:06', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ebook_category`
--

CREATE TABLE `ebook_category` (
  `ebook_category_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `description` text,
  `img` varchar(255) DEFAULT 'default-img.png',
  `is_active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ebook_category`
--

INSERT INTO `ebook_category` (`ebook_category_id`, `category_name`, `link`, `description`, `img`, `is_active`) VALUES
(2, 'MOTIFASI', 'motifasi', '<p>Motifasi</p>', 'default-img.png', 1),
(3, 'Agama', 'agama', '<p>Agama</p>', 'default-img.png', 1),
(4, 'Sosial', 'sosial', '<p>Sosial</p>', 'default-img.png', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gallery`
--

CREATE TABLE `gallery` (
  `gallery_id` int(11) NOT NULL,
  `gallery_name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_datetime` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `is_publish` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gallery`
--

INSERT INTO `gallery` (`gallery_id`, `gallery_name`, `description`, `created_datetime`, `created_by`, `link`, `is_publish`) VALUES
(1, 'GALERY 1', 'ABCDE GALERY 1', '2017-06-06 10:37:28', 2, 'galery-1', 1),
(2, 'Gallery 2', '<p>ini gallery 2</p>', '2017-06-06 11:26:19', 2, 'gallery-2', 1),
(3, 'GALLERY 3', '<p>ini galerry 3</p>', '2017-06-06 11:27:38', 2, 'gallery-3', 1),
(4, 'IVEN KAMPUS', '<p>IVEN KAMPUS</p>', '2017-06-06 22:26:46', 2, 'iven-kampus', 1),
(5, 'GALERY TEST', 'test', '2017-07-15 12:50:32', 2, 'galery-test', 1),
(6, 'ggg', 'ggg', '2017-07-15 16:20:42', 2, 'ggg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gallery_detail`
--

CREATE TABLE `gallery_detail` (
  `gallery_detail_id` int(11) NOT NULL,
  `gallery_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `description` text,
  `is_active` tinyint(1) DEFAULT '1',
  `alt` varchar(255) DEFAULT NULL,
  `is_cover` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gallery_detail`
--

INSERT INTO `gallery_detail` (`gallery_detail_id`, `gallery_id`, `title`, `img`, `description`, `is_active`, `alt`, `is_cover`) VALUES
(17, 1, 'hhh', 'logo.jpg', 'hhh', 1, 'ttt', 1),
(18, 1, 'hhh', 'logo-image.jpg', 'hhh', 1, 'ttt', 0),
(19, 1, 'hhh', 'logo1.jpg', 'hhh', 1, 'ttt', 0),
(20, 2, 'aa', 'logo-image1.jpg', 'aa', 1, 'aa', 1),
(21, 2, 'aa', 'logo2.jpg', 'aa', 1, 'aa', 0),
(22, 3, '', 'logo3.jpg', '', 1, '', 0),
(23, 3, '', 'logo-image2.jpg', '', 1, '', 1),
(24, 4, 'smk garuda purwakarta', 'smk_garuda_purwakarta1.JPG', 'smk garuda purwakarta', 1, '', 0),
(25, 4, 'SMAN 1 PATOKBEUSI', 'SMAN_1_PATOKBEUSI1.JPG', 'SMAN 1 PATOKBEUSI', 1, '', 0),
(26, 4, 'INAUGURASI FKIP-UNSIKA', 'INAUGURASI_FKIP-UNSIKA1.JPG', 'INAUGURASI FKIP-UNSIKA', 1, '', 1),
(27, 4, 'SMKN 1 TIRTAJAYA', 'SMKN_1_TIRTAJAYA1.JPG', 'SMKN 1 TIRTAJAYA', 1, '', 0),
(28, 5, 'ss', 'phone.png', 'ss', 1, '', 1),
(29, 6, 'd', 'slide21.jpg', 'd', 1, '', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `guestbook`
--

CREATE TABLE `guestbook` (
  `guestbook_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `created_datetime` datetime DEFAULT NULL,
  `is_processed` tinyint(1) DEFAULT '0',
  `company` varchar(255) DEFAULT NULL,
  `address` text,
  `type` enum('guestbook','portfolio','lamaran','') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `guestbook`
--

INSERT INTO `guestbook` (`guestbook_id`, `title`, `content`, `name`, `email`, `phone`, `created_datetime`, `is_processed`, `company`, `address`, `type`) VALUES
(1, 'idiejidfj', '<p>dwdwd</p>', 'qq', 'qq@mail.com', '099889', '2017-06-16 08:33:09', 0, NULL, NULL, NULL),
(2, 'Guestbook', '<p>SSDSDSDSDSD</p>', 'aku', 'aku@mail.com', NULL, '2017-06-16 09:15:22', 0, NULL, NULL, 'guestbook'),
(3, 'Guestbook', '<p>SSD</p>', 'SS', 'AA@MAIL.COM', NULL, '2017-06-16 09:28:50', 0, NULL, NULL, 'guestbook'),
(4, 'Guestbook', '<p>QSQS</p>', 'QWQ', 'AA@MAIL.COM', NULL, '2017-06-16 09:35:35', 0, NULL, NULL, 'guestbook'),
(5, 'Guestbook', '<p>assdaa</p>', 'asa', 'FFGF@MAIL.COM', NULL, '2017-06-16 09:41:57', 0, NULL, NULL, 'guestbook'),
(6, 'Guestbook', '<p>sasa</p>', 'aa', 'AA@MAIL.COM', NULL, '2017-06-16 09:47:24', 0, NULL, NULL, 'guestbook'),
(7, 'Guestbook', '<p>dhduudfedfe</p>', 'sasa', 'sssa@kdsj.com', NULL, '2017-06-16 10:07:59', 0, NULL, NULL, 'guestbook'),
(8, 'Guestbook', 'ddfd', 'dd', 'FFGF@MAIL.COM', NULL, '2017-06-16 20:35:53', 0, NULL, NULL, 'guestbook'),
(9, 'Guestbook', 'xsxs', 'AA', 'FFGF@MAIL.COM', NULL, '2017-06-16 20:46:05', 0, NULL, NULL, 'guestbook'),
(10, 'Guestbook', 'xsxs', 'xs', 'FFGF@MAIL.COM', NULL, '2017-06-16 20:47:15', 0, NULL, NULL, 'guestbook'),
(11, 'Guestbook', 'sasa', 'sas', 'FFGF@MAIL.COM', NULL, '2017-06-16 20:50:06', 0, NULL, NULL, 'guestbook'),
(12, 'Guestbook', 'sds', 'ds', 'sssa@kdsj.com', NULL, '2017-06-16 21:00:22', 0, NULL, NULL, 'guestbook'),
(13, 'Guestbook', 'dsds', 'AA', 'FFGF@MAIL.COM', NULL, '2017-06-16 21:08:00', 0, NULL, NULL, 'guestbook'),
(14, 'Guestbook', 'dsdss', 'ds', 'AA@MAIL.COM', NULL, '2017-06-16 21:11:52', 0, NULL, NULL, 'guestbook'),
(15, 'Guestbook', 'r3r23re', 'rew', 'sssa@kdsj.com', NULL, '2017-06-16 21:13:30', 0, NULL, NULL, 'guestbook'),
(16, 'Guestbook', 'dsas', 'sds', 'sssa@kdsj.com', NULL, '2017-06-16 21:30:12', 0, NULL, NULL, 'guestbook'),
(17, 'Guestbook', 'dhsjdfjs', 'sas', 'sss@kdfk.com', NULL, '2017-06-16 21:35:20', 0, NULL, NULL, 'guestbook'),
(18, 'Guestbook', 'sds', 'sds', 'AA@MAIL.COM', NULL, '2017-06-16 21:35:32', 0, NULL, NULL, 'guestbook'),
(19, 'Guestbook', 'dsds', 'test', 'FFGF@MAIL.COM', NULL, '2017-06-19 08:07:50', 0, NULL, NULL, 'guestbook'),
(20, 'Guestbook', 'scscs', 'halow', 'a@mail.com', NULL, '2017-06-19 08:10:27', 0, NULL, NULL, 'guestbook'),
(21, 'Guestbook', 'jdjws', 'ijid', 'FFGF@MAIL.COM', NULL, '2017-07-06 21:57:04', 0, NULL, NULL, 'guestbook'),
(22, 'Guestbook', 'dddd', 'ss', 'wachidrudyanto@gmail.com', NULL, '2017-07-12 16:25:17', 0, NULL, NULL, 'guestbook'),
(23, 'Guestbook', 'jjhjhj', 'aaa', 'wachidrudyanto@gmail.com', NULL, '2017-07-14 21:55:32', 0, NULL, NULL, 'guestbook'),
(24, 'Guestbook', 'jjhjhj', 'aaa', 'wachidrudyanto@gmail.com', NULL, '2017-07-14 21:55:53', 0, NULL, NULL, 'guestbook'),
(25, 'Guestbook', 'jjhjhj', 'aaa', 'wachidrudyanto@gmail.com', NULL, '2017-07-14 22:02:43', 0, NULL, NULL, 'guestbook'),
(26, 'Guestbook', 'jjhjhj', 'aaa', 'wachidrudyanto@gmail.com', NULL, '2017-07-14 22:36:16', 0, NULL, NULL, 'guestbook'),
(27, 'Guestbook', 'jjhjhj', 'aaa', 'wachidrudyanto@gmail.com', NULL, '2017-07-14 22:40:15', 0, NULL, NULL, 'guestbook'),
(28, 'Guestbook', 'jjhjhj', 'aaa', 'wachidrudyanto@gmail.com', NULL, '2017-07-14 23:10:06', 0, NULL, NULL, 'guestbook'),
(29, 'Guestbook', 'ssq', 'wachid', 'wachidrudyanto@gmail.com', NULL, '2017-07-14 23:10:35', 0, NULL, NULL, 'guestbook');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal_dokter`
--

CREATE TABLE `jadwal_dokter` (
  `id_jadwal` int(11) NOT NULL,
  `hari` varchar(10) NOT NULL,
  `jam` time NOT NULL,
  `id_dokter` varchar(255) DEFAULT NULL,
  `spesialis` varchar(255) DEFAULT NULL,
  `id_rumahsakit` int(11) NOT NULL,
  `ket` varchar(200) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwal_dokter`
--

INSERT INTO `jadwal_dokter` (`id_jadwal`, `hari`, `jam`, `id_dokter`, `spesialis`, `id_rumahsakit`, `ket`, `is_active`) VALUES
(1, 'Senin', '00:00:06', '0', 'Dokter Umum', 1, '<p>ok</p>\r\n', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `m_user`
--

CREATE TABLE `m_user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(64) DEFAULT NULL,
  `password` varchar(100) DEFAULT '5600e318a8eb1b54a5dcb60c3a793eaa892f34f2b0d5c3e6145bbdd979907e2f',
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `level` enum('admin','user') DEFAULT 'user',
  `is_active` tinyint(1) DEFAULT '1',
  `img` varchar(255) DEFAULT 'default-user.png',
  `position` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `m_user`
--

INSERT INTO `m_user` (`user_id`, `username`, `password`, `name`, `email`, `phone`, `level`, `is_active`, `img`, `position`) VALUES
(1, 'aris', '5600e318a8eb1b54a5dcb60c3a793eaa892f34f2b0d5c3e6145bbdd979907e2f', 'Aris Priyanto', 'aris@aabc-software.com', '081906660xxx', 'admin', 1, 'Desert.jpg', 'Marketing Manager'),
(2, 'admin', '5600e318a8eb1b54a5dcb60c3a793eaa892f34f2b0d5c3e6145bbdd979907e2f', 'WACHID', 'info@publicspeakingsbc.com', '081000000000000', 'admin', 1, 'Koala.jpg', 'Marketing Manager'),
(3, 'agus', '5600e318a8eb1b54a5dcb60c3a793eaa892f34f2b0d5c3e6145bbdd979907e2f', 'Agus Badrussalam', 'agus.badruss@gmail.com', '08389856364', 'admin', 1, 'default-user.png', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `page`
--

CREATE TABLE `page` (
  `page_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `content` text,
  `img` varchar(255) DEFAULT 'default-page.png',
  `is_publish` tinyint(1) DEFAULT '1',
  `created_datetime` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_datetime` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `counter` int(11) DEFAULT '1',
  `is_removeable` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `page`
--

INSERT INTO `page` (`page_id`, `title`, `link`, `content`, `img`, `is_publish`, `created_datetime`, `created_by`, `updated_datetime`, `updated_by`, `counter`, `is_removeable`) VALUES
(1, 'TENTANG KAMI', 'profile', '<div style=\"background:#eeeeee;border:1px solid #cccccc;padding:5px 10px;\"><span style=\"font-size:20px\"><strong>Soul Brain Communication (SBC)</strong></span></div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Public Speaking &amp; Motivational School PERTAMA DI KARAWANG, serta merupakan salah satu pusat pengembangan diriyang memiliki program-programunggulan yang dapat meningkatkan softskill maupun hardskill.</p>\r\n\r\n<p>Dengan banyaknya pengalaman di bidang peatihan SBC siap membantu komunitas, lembaga, sekolah, kampus, instansi maupun umum untuk lebih mengoptimalkan sumber daya manusia agar menjadi pribadi yang terampil, tanggungjawab, kreatif, komunikatif dan unggul.</p>\r\n\r\n<ul>\r\n	<li><span style=\"font-size:18px\"><big><strong>VISI</strong></big></span></li>\r\n</ul>\r\n\r\n<p style=\"margin-left:40px\">&quot;Menjadi institusi yang menghasilkan publick spaker yang unggul dan berkepribadian baik.&quot;</p>\r\n\r\n<ul>\r\n	<li><strong><span style=\"font-size:18px\"><big>MISI</big></span></strong></li>\r\n</ul>\r\n\r\n<p style=\"margin-left:40px\">&quot;Menyelenggarakan pelatihan public speaking berbasis pengembangan diri&quot;</p>', 'ppp1.png', 1, '2017-04-20 23:17:26', 1, '2017-07-11 14:59:53', 2, 1, 0),
(2, 'TRAINING', 'training', '<p>Kami dari SOUL BRAIN COMMUNICATION (SBC) mempunyai beberapa resep jitu untuk meningkatkan motivasi kerja perusahaan Anda.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div style=\"background:#eeeeee;border:1px solid #cccccc;padding:5px 10px;\">&nbsp;<strong>Pelatihan Great Public Speaking Plus (GPS +) Batch 9</strong>&nbsp;</div>\r\n\r\n<p style=\"margin-left:40px\"><strong>Hari/Tanggal &nbsp;&nbsp; : Minggu, </strong><strong>3 Desember 2017</strong></p>\r\n\r\n<p style=\"margin-left:40px\"><strong>Tempat&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; :&nbsp; Hotel</strong><strong> Mercure</strong><strong> Karawang</strong></p>\r\n\r\n<p style=\"margin-left:40px\"><strong>Waktu&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 08.00-16.00 WIB</strong></p>\r\n\r\n<p style=\"margin-left:40px\"><strong>Biaya&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; : Normal Rp. 700.000</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><em>Early Bird Rp 300.000 (</em></strong><strong><em>sampai akhir bulan Oktober</em></strong><strong><em>)</em></strong></p>\r\n\r\n<p><strong><em>Pendaftaran setelah itu biayanya kembali normal RP. 700.000</em></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Cara Pendaftaran:</p>\r\n\r\n<p style=\"margin-left:36.0pt\">Ketik nama/pelatihan/profesi/no hp</p>\r\n\r\n<p>Contoh: &nbsp;&nbsp;&nbsp;Anisa/GPS +/Mahasiswa/085691141196</p>\r\n\r\n<p>Kirim ke:&nbsp; 085814313888</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Transfer Pembayaran:</strong></p>\r\n\r\n<p style=\"margin-left:35.45pt\"><strong>a.n. Yousef Bani Ahmad</strong></p>\r\n\r\n<p style=\"margin-left:35.45pt\"><strong>Bank BCA</strong></p>\r\n\r\n<p style=\"margin-left:35.45pt\"><strong>No. Rekening: </strong><strong>3423183710</strong></p>\r\n\r\n<p style=\"margin-left:35.45pt\"><strong>(Bukti transfer dibawa pada saat pelatihan)</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div style=\"background:#eeeeee;border:1px solid #cccccc;padding:5px 10px;\"><strong>Pelatihan Serifikasi Hypnotist &amp; Hypnotherapist (CH., C.Ht)</strong></div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Hari/Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Sabtu-Minggu, </strong><strong>29-30 Juli 2017</strong><strong>&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>\r\n\r\n<p style=\"margin-left:36.0pt\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;Sabtu-Minggu,&nbsp; 26-27 </strong><strong>Agustus 2017</strong>&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p style=\"margin-left:78.0pt\">&nbsp; <strong>S</strong><strong>abtu-Minggu,&nbsp; 7-8 Oktober 2017</strong></p>\r\n\r\n<p style=\"margin-left:78.0pt\"><strong>&nbsp; S</strong><strong>abtu-Minggu,&nbsp; </strong><strong>11-12 November 2017</strong></p>\r\n\r\n<p style=\"margin-left:78.0pt\"><strong>&nbsp; S</strong><strong>abtu-Minggu,&nbsp; </strong><strong>9-10 Desember 2017</strong></p>\r\n\r\n<p><strong>Tempat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </strong><strong>Brits Hotel</strong></p>\r\n\r\n<p><strong>Waktu&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 09.00-17.00 WIB</strong></p>\r\n\r\n<p><strong>Biaya&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Normal 3.750.000</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><em>Early Bird 2.500.000 (pembayaran 1 minggu sebelum acara)</em></strong></p>\r\n\r\n<p><strong><em>Booking Seat Rp. 500.000.,</em></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Cara Pendaftaran:</p>\r\n\r\n<p style=\"margin-left:36.0pt\">Ketik nama/pelatihan /profesi/no hp</p>\r\n\r\n<p>Contoh:&nbsp;&nbsp;&nbsp; Lusi/ hypnotist/Pengusaha/085691141196</p>\r\n\r\n<p>Kirim ke:&nbsp; 085814313888</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Transfer Pembayaran:</strong></p>\r\n\r\n<p style=\"margin-left:35.45pt\"><strong>a.n. Yousef Bani Ahmad</strong></p>\r\n\r\n<p style=\"margin-left:35.45pt\"><strong>Bank BCA</strong></p>\r\n\r\n<p style=\"margin-left:35.45pt\"><strong>No. Rekening: </strong><strong>3423183710</strong></p>\r\n\r\n<p style=\"margin-left:35.45pt\"><strong>(Bukti transfer dibawa pada saat pelatihan)</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div style=\"background:#eeeeee;border:1px solid #cccccc;padding:5px 10px;\"><strong>Pelatihan Serifikasi </strong><strong>Neuro Linguistic Programming (NLP)</strong></div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Hari/Tanggal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>\r\n\r\n<p style=\"margin-left:36.0pt\"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;Sabtu-Minggu,&nbsp; 12-13 </strong><strong>Agustus 2017</strong>&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p style=\"margin-left:78.0pt\">&nbsp; <strong>S</strong><strong>abtu-Minggu,&nbsp; </strong><strong>23-24 September</strong><strong> 2017</strong></p>\r\n\r\n<p style=\"margin-left:78.0pt\"><strong>&nbsp; S</strong><strong>abtu-Minggu,&nbsp; </strong><strong>21-22 Oktober 2017</strong></p>\r\n\r\n<p style=\"margin-left:78.0pt\"><strong>&nbsp; S</strong><strong>abtu-Minggu,&nbsp; </strong><strong>25-26 November 2017</strong></p>\r\n\r\n<p><strong>Tempat&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : </strong><strong>Brits Hotel</strong></p>\r\n\r\n<p><strong>Waktu&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : 09.00-17.00 WIB</strong></p>\r\n\r\n<p><strong>Biaya&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : Normal 2.750.000</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong><em>Early Bird 1.800.000 (pembayaran 1 minggu sebelum acara)</em></strong></p>\r\n\r\n<p><strong><em>Booking Seat Rp. 500.000.,</em></strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Cara Pendaftaran:</p>\r\n\r\n<p style=\"margin-left:36.0pt\">Ketik nama/pelatihan /profesi/no hp</p>\r\n\r\n<p>Contoh:&nbsp;&nbsp;&nbsp; Lusi/ NLP/Pengusaha/085691141196</p>\r\n\r\n<p>Kirim ke:&nbsp; 085814313888</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Transfer Pembayaran:</strong></p>\r\n\r\n<p style=\"margin-left:35.45pt\"><strong>a.n. Yousef Bani Ahmad</strong></p>\r\n\r\n<p style=\"margin-left:35.45pt\"><strong>Bank BCA</strong></p>\r\n\r\n<p style=\"margin-left:35.45pt\"><strong>No. Rekening: </strong><strong>3423183710</strong></p>\r\n\r\n<p style=\"margin-left:35.45pt\"><strong>(Bukti transfer dibawa pada saat pelatihan)</strong></p>', 'profile_-_Copy_copy1.png', 1, '2017-06-07 12:39:02', 2, '2017-07-12 13:06:38', 2, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rumah_sakit`
--

CREATE TABLE `rumah_sakit` (
  `id_rumahsakit` int(11) NOT NULL,
  `nama_rumahsakit` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rumah_sakit`
--

INSERT INTO `rumah_sakit` (`id_rumahsakit`, `nama_rumahsakit`, `alamat`, `is_active`) VALUES
(1, 'rspk lippo', '<p>lippo</p>', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `setting`
--

CREATE TABLE `setting` (
  `setting_id` int(11) NOT NULL,
  `name_set` varchar(255) DEFAULT NULL,
  `value_set` text,
  `description` text,
  `is_active` tinyint(1) DEFAULT '1',
  `is_system` tinyint(1) DEFAULT '0',
  `is_removeable` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `setting`
--

INSERT INTO `setting` (`setting_id`, `name_set`, `value_set`, `description`, `is_active`, `is_system`, `is_removeable`) VALUES
(1, 'APP_VERSION', '<p>1.0 beta</p>', 'Application Version', 1, 1, 0),
(2, 'SITE_DESCRIPTION', 'Official Website SBC', 'Deskripsi Website', 1, 0, 0),
(6, 'telp', '+62 267 999999', 'No. telp', 1, 0, 1),
(7, 'email-info', 'info@publicspeakingsbc.com', 'email-info', 1, 0, 1),
(8, 'address1', 'Ruko Mahkota Regency (dekat kampus UNSIKA)\r\nNo 3/11. Telukjambe Timur, Karawang\r\nKarawang\r\ninfo@sbc.com', 'alamat 01', 1, 0, 1),
(9, 'email_receiver', 'info@publicspeakingsbc.com', 'Emain penerima notifikasi (jika lebih dari satu pisahkan dengan koma , )', 1, 0, 0),
(11, 'WA01', '085774700950', 'WA01', 1, 0, 1),
(12, 'WA02', '085793729693', 'WA02', 1, 0, 1),
(13, 'instagram', '#sbc_karawang', 'Instagram', 1, 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `slide`
--

CREATE TABLE `slide` (
  `slide_id` int(11) NOT NULL,
  `img` varchar(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `alt` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `slide`
--

INSERT INTO `slide` (`slide_id`, `img`, `title`, `description`, `alt`, `link`, `is_active`) VALUES
(20, 'slide211.jpg', 'Slide 1', 'Desc Slide 1', '', NULL, 1),
(21, 'slide31.jpg', 'Slide 2', 'Desc Slide 2', '', NULL, 1),
(22, 'Koala.jpg', 'slide 3', 'desc slide 3', '', NULL, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_article`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_article` (
`article_id` int(11)
,`title` varchar(255)
,`link` varchar(255)
,`content` text
,`img` varchar(255)
,`article_category_id` int(11)
,`category_name` varchar(255)
,`created_by` int(11)
,`username` varchar(64)
,`name` varchar(255)
,`created_datetime` datetime
,`updated_by` int(11)
,`updated_datetime` datetime
,`is_publish` tinyint(1)
,`counter` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_comment`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_comment` (
`link` varchar(255)
,`article_content` text
,`coment_id` int(11)
,`article_id` int(11)
,`parent_coment_id` int(11)
,`name` varchar(255)
,`email` varchar(255)
,`content` text
,`is_approve` tinyint(1)
,`created_datetime` datetime
,`img` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_dokter`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_dokter` (
`id_dokter` int(11)
,`nama_dokter` varchar(255)
,`img` varchar(255)
,`alamat` varchar(255)
,`bagian` varchar(150)
,`id_rumahsakit` int(11)
,`ket` varchar(255)
,`is_active` tinyint(1)
,`nama_rumahsakit` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_download`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_download` (
`download_id` int(11)
,`name` varchar(200)
,`email` varchar(255)
,`ket` text
,`ebook_id` int(11)
,`download_datetime` datetime
,`ebook_title` varchar(255)
,`ebook_ket` text
,`category_ebook_name` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_ebook`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_ebook` (
`ebook_id` int(11)
,`title` varchar(255)
,`link` varchar(255)
,`ket` text
,`img` varchar(255)
,`ebook_category_id` int(11)
,`created_by` int(11)
,`created_datetime` datetime
,`updated_by` int(11)
,`updated_datetime` datetime
,`is_publish` tinyint(1)
,`category_name` varchar(255)
,`name_create` varchar(255)
,`name_update` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_gallery`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_gallery` (
`gallery_id` int(11)
,`gallery_name` varchar(255)
,`created_datetime` datetime
,`created_by` int(11)
,`link` varchar(255)
,`is_publish` tinyint(1)
,`gallery_detail_id` int(11)
,`title` varchar(255)
,`img` varchar(255)
,`description` text
,`is_active` tinyint(1)
,`alt` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_gallery_header`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_gallery_header` (
`gallery_id` int(11)
,`gallery_name` varchar(255)
,`description` text
,`created_datetime` datetime
,`created_by` int(11)
,`link` varchar(255)
,`is_publish` tinyint(1)
,`cover` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_jadwaldokter`
-- (Lihat di bawah untuk tampilan aktual)
--
CREATE TABLE `v_jadwaldokter` (
`id_jadwal` int(11)
,`hari` varchar(10)
,`jam` time
,`id_dokter` varchar(255)
,`spesialis` varchar(255)
,`id_rumahsakit` int(11)
,`ket` varchar(200)
,`is_active` tinyint(1)
,`nama_dokter` varchar(255)
,`nama_rumahsakit` varchar(255)
);

-- --------------------------------------------------------

--
-- Struktur untuk view `v_article`
--
DROP TABLE IF EXISTS `v_article`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_article`  AS  select `a`.`article_id` AS `article_id`,`a`.`title` AS `title`,`a`.`link` AS `link`,`a`.`content` AS `content`,`a`.`img` AS `img`,`a`.`article_category_id` AS `article_category_id`,`c`.`category_name` AS `category_name`,`a`.`created_by` AS `created_by`,`u`.`username` AS `username`,`u`.`name` AS `name`,`a`.`created_datetime` AS `created_datetime`,`a`.`updated_by` AS `updated_by`,`a`.`updated_datetime` AS `updated_datetime`,`a`.`is_publish` AS `is_publish`,`a`.`counter` AS `counter` from ((`article` `a` left join `article_category` `c` on((`a`.`article_category_id` = `c`.`article_category_id`))) left join `m_user` `u` on((`a`.`created_by` = `u`.`user_id`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_comment`
--
DROP TABLE IF EXISTS `v_comment`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_comment`  AS  select `a`.`link` AS `link`,`a`.`content` AS `article_content`,`c`.`coment_id` AS `coment_id`,`c`.`article_id` AS `article_id`,`c`.`parent_coment_id` AS `parent_coment_id`,`c`.`name` AS `name`,`c`.`email` AS `email`,`c`.`content` AS `content`,`c`.`is_approve` AS `is_approve`,`c`.`created_datetime` AS `created_datetime`,`c`.`img` AS `img` from (`coment` `c` join `article` `a` on((`c`.`article_id` = `a`.`article_id`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_dokter`
--
DROP TABLE IF EXISTS `v_dokter`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_dokter`  AS  select `dok`.`id_dokter` AS `id_dokter`,`dok`.`nama_dokter` AS `nama_dokter`,`dok`.`img` AS `img`,`dok`.`alamat` AS `alamat`,`dok`.`bagian` AS `bagian`,`dok`.`id_rumahsakit` AS `id_rumahsakit`,`dok`.`ket` AS `ket`,`dok`.`is_active` AS `is_active`,`rs`.`nama_rumahsakit` AS `nama_rumahsakit` from (`dokter` `dok` left join `rumah_sakit` `rs` on((`dok`.`id_rumahsakit` = `rs`.`id_rumahsakit`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_download`
--
DROP TABLE IF EXISTS `v_download`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_download`  AS  select `d`.`download_id` AS `download_id`,`d`.`name` AS `name`,`d`.`email` AS `email`,`d`.`ket` AS `ket`,`d`.`ebook_id` AS `ebook_id`,`d`.`download_datetime` AS `download_datetime`,`e`.`title` AS `ebook_title`,`e`.`ket` AS `ebook_ket`,`e`.`category_name` AS `category_ebook_name` from (`download_history` `d` join `v_ebook` `e` on((`e`.`ebook_id` = `d`.`ebook_id`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_ebook`
--
DROP TABLE IF EXISTS `v_ebook`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_ebook`  AS  select `e`.`ebook_id` AS `ebook_id`,`e`.`title` AS `title`,`e`.`link` AS `link`,`e`.`ket` AS `ket`,`e`.`img` AS `img`,`e`.`ebook_category_id` AS `ebook_category_id`,`e`.`created_by` AS `created_by`,`e`.`created_datetime` AS `created_datetime`,`e`.`updated_by` AS `updated_by`,`e`.`updated_datetime` AS `updated_datetime`,`e`.`is_publish` AS `is_publish`,`c`.`category_name` AS `category_name`,`u`.`name` AS `name_create`,(select `mu`.`name` from `m_user` `mu` where (`mu`.`user_id` = `e`.`updated_by`)) AS `name_update` from ((`ebook` `e` join `ebook_category` `c` on((`e`.`ebook_category_id` = `c`.`ebook_category_id`))) join `m_user` `u` on((`u`.`user_id` = `e`.`created_by`))) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_gallery`
--
DROP TABLE IF EXISTS `v_gallery`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_gallery`  AS  select `h`.`gallery_id` AS `gallery_id`,`h`.`gallery_name` AS `gallery_name`,`h`.`created_datetime` AS `created_datetime`,`h`.`created_by` AS `created_by`,`h`.`link` AS `link`,`h`.`is_publish` AS `is_publish`,`d`.`gallery_detail_id` AS `gallery_detail_id`,`d`.`title` AS `title`,`d`.`img` AS `img`,`d`.`description` AS `description`,`d`.`is_active` AS `is_active`,`d`.`alt` AS `alt` from (`gallery` `h` join `gallery_detail` `d`) where (`h`.`gallery_id` = `d`.`gallery_id`) ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_gallery_header`
--
DROP TABLE IF EXISTS `v_gallery_header`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_gallery_header`  AS  select `g`.`gallery_id` AS `gallery_id`,`g`.`gallery_name` AS `gallery_name`,`g`.`description` AS `description`,`g`.`created_datetime` AS `created_datetime`,`g`.`created_by` AS `created_by`,`g`.`link` AS `link`,`g`.`is_publish` AS `is_publish`,(case when isnull((select `d`.`img` from `gallery_detail` `d` where ((`d`.`gallery_id` = `g`.`gallery_id`) and (`d`.`is_cover` = 1) and (`d`.`is_active` = 1)) limit 1)) then 'default-image.png' else (select `e`.`img` from `gallery_detail` `e` where ((`e`.`gallery_id` = `g`.`gallery_id`) and (`e`.`is_cover` = 1) and (`e`.`is_active` = 1)) limit 1) end) AS `cover` from `gallery` `g` ;

-- --------------------------------------------------------

--
-- Struktur untuk view `v_jadwaldokter`
--
DROP TABLE IF EXISTS `v_jadwaldokter`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_jadwaldokter`  AS  select `j`.`id_jadwal` AS `id_jadwal`,`j`.`hari` AS `hari`,`j`.`jam` AS `jam`,`j`.`id_dokter` AS `id_dokter`,`j`.`spesialis` AS `spesialis`,`j`.`id_rumahsakit` AS `id_rumahsakit`,`j`.`ket` AS `ket`,`j`.`is_active` AS `is_active`,`d`.`nama_dokter` AS `nama_dokter`,`r`.`nama_rumahsakit` AS `nama_rumahsakit` from ((`jadwal_dokter` `j` join `dokter` `d` on((`j`.`id_dokter` = `d`.`id_dokter`))) join `rumah_sakit` `r` on((`j`.`id_rumahsakit` = `r`.`id_rumahsakit`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`article_id`),
  ADD UNIQUE KEY `link` (`link`),
  ADD KEY `article_category_id` (`article_category_id`,`created_by`,`updated_by`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `id_rumahsakit` (`id_rumahsakit`);

--
-- Indexes for table `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`article_category_id`),
  ADD UNIQUE KEY `link` (`link`);

--
-- Indexes for table `coment`
--
ALTER TABLE `coment`
  ADD PRIMARY KEY (`coment_id`),
  ADD KEY `article_id` (`article_id`,`parent_coment_id`),
  ADD KEY `parent_coment_id` (`parent_coment_id`);

--
-- Indexes for table `dokter`
--
ALTER TABLE `dokter`
  ADD PRIMARY KEY (`id_dokter`),
  ADD KEY `id_rumahsakit` (`id_rumahsakit`);

--
-- Indexes for table `download_history`
--
ALTER TABLE `download_history`
  ADD PRIMARY KEY (`download_id`),
  ADD KEY `ebook_id` (`ebook_id`);

--
-- Indexes for table `ebook`
--
ALTER TABLE `ebook`
  ADD PRIMARY KEY (`ebook_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `update_by` (`updated_by`),
  ADD KEY `ebook_category_id` (`ebook_category_id`);

--
-- Indexes for table `ebook_category`
--
ALTER TABLE `ebook_category`
  ADD PRIMARY KEY (`ebook_category_id`),
  ADD KEY `link` (`link`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`gallery_id`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `gallery_detail`
--
ALTER TABLE `gallery_detail`
  ADD PRIMARY KEY (`gallery_detail_id`),
  ADD KEY `gallery_id` (`gallery_id`);

--
-- Indexes for table `guestbook`
--
ALTER TABLE `guestbook`
  ADD PRIMARY KEY (`guestbook_id`);

--
-- Indexes for table `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- Indexes for table `m_user`
--
ALTER TABLE `m_user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`page_id`),
  ADD UNIQUE KEY `link` (`link`),
  ADD KEY `created_by` (`created_by`,`updated_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `rumah_sakit`
--
ALTER TABLE `rumah_sakit`
  ADD PRIMARY KEY (`id_rumahsakit`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`setting_id`),
  ADD UNIQUE KEY `name_set` (`name_set`);

--
-- Indexes for table `slide`
--
ALTER TABLE `slide`
  ADD PRIMARY KEY (`slide_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `article_category`
--
ALTER TABLE `article_category`
  MODIFY `article_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `coment`
--
ALTER TABLE `coment`
  MODIFY `coment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `download_history`
--
ALTER TABLE `download_history`
  MODIFY `download_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `ebook`
--
ALTER TABLE `ebook`
  MODIFY `ebook_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `ebook_category`
--
ALTER TABLE `ebook_category`
  MODIFY `ebook_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `gallery_detail`
--
ALTER TABLE `gallery_detail`
  MODIFY `gallery_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `guestbook`
--
ALTER TABLE `guestbook`
  MODIFY `guestbook_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_user`
--
ALTER TABLE `m_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `page`
--
ALTER TABLE `page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `rumah_sakit`
--
ALTER TABLE `rumah_sakit`
  MODIFY `id_rumahsakit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `slide`
--
ALTER TABLE `slide`
  MODIFY `slide_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`article_category_id`) REFERENCES `article_category` (`article_category_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `article_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `m_user` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `article_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `m_user` (`user_id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `coment`
--
ALTER TABLE `coment`
  ADD CONSTRAINT `coment_ibfk_1` FOREIGN KEY (`article_id`) REFERENCES `article` (`article_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `coment_ibfk_2` FOREIGN KEY (`parent_coment_id`) REFERENCES `coment` (`coment_id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `dokter`
--
ALTER TABLE `dokter`
  ADD CONSTRAINT `dokter_ibfk_1` FOREIGN KEY (`id_rumahsakit`) REFERENCES `rumah_sakit` (`id_rumahsakit`);

--
-- Ketidakleluasaan untuk tabel `download_history`
--
ALTER TABLE `download_history`
  ADD CONSTRAINT `download_history_ibfk_1` FOREIGN KEY (`ebook_id`) REFERENCES `ebook` (`ebook_id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `ebook`
--
ALTER TABLE `ebook`
  ADD CONSTRAINT `ebook_ibfk_1` FOREIGN KEY (`ebook_category_id`) REFERENCES `ebook_category` (`ebook_category_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ebook_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `m_user` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ebook_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `m_user` (`user_id`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `page`
--
ALTER TABLE `page`
  ADD CONSTRAINT `page_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `m_user` (`user_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `page_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `m_user` (`user_id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
