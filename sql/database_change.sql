--ubah v_article

drop view v_article;
create view v_article as
SELECT 
        `a`.`article_id` AS `article_id`,
        `a`.`title` AS `title`,
        `a`.`link` AS `link`,
        `a`.`content` AS `content`,
        `a`.`img` AS `img`,
        `a`.`article_category_id` AS `article_category_id`,
        `c`.`category_name` AS `category_name`,
        `a`.`created_by` AS `created_by`,
        `u`.`username` AS `username`,
        `u`.`name` AS `name`,
        `a`.`created_datetime` AS `created_datetime`,
        `a`.`updated_by` AS `updated_by`,
        `a`.`updated_datetime` AS `updated_datetime`,
        `a`.`is_publish` AS `is_publish`,
        `a`.`counter` AS `counter`,
        r.nama_rumahsakit
    FROM
        ((`article` `a`
        INNER JOIN `article_category` `c` ON ((`a`.`article_category_id` = `c`.`article_category_id`)))
         INNER JOIN rumah_sakit r on a.id_rumahsakit=r.id_rumahsakit
        LEFT JOIN `m_user` `u` ON ((`a`.`created_by` = `u`.`user_id`)))