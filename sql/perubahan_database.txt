-------------------------------------------------------------------------------------------------------------------
------------------------------------------perubahan database-------------------------------------------------------
---------------------------------kalau sudah di update silahkan dihapus--------------------------------------------
-------------------------------------------------------------------------------------------------------------------

--------------------------------tambah view dokter----------------------------------------------------

CREATE view v_dokter as SELECT dok.*,rs.nama_rumahsakit FROM `dokter` dok left join rumah_sakit rs on dok.`id_rumahsakit` = rs.id_rumahsakit

-----------------------------------------------------------------------------------------------------

-----------------------------------tambah view jadwal dokter--------------------------------------------------------

create view v_jadwaldokter as SELECT j.*, d.nama_dokter,r.nama_rumahsakit from jadwal_dokter j INNER join dokter d on j.id_dokter=d.id_dokter INNER join rumah_sakit r on j.id_rumahsakit=r.id_rumahsakit

---------------------------------------------------------------------------------------------------------


-------------------------ubah tabel jadwal dokter---------------------

DROP TABLE IF EXISTS `jadwal_dokter`;
CREATE TABLE `jadwal_dokter` (
  `id_jadwal` int(11) NOT NULL,
  `hari` varchar(10) NOT NULL,
  `jam` time NOT NULL,
  `id_dokter` varchar(255) DEFAULT NULL,
  `spesialis` varchar(255) DEFAULT NULL,
  `id_rumahsakit` int(11) NOT NULL,
  `ket` varchar(200) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  ADD PRIMARY KEY (`id_jadwal`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jadwal_dokter`
--
ALTER TABLE `jadwal_dokter`
  MODIFY `id_jadwal` int(11) NOT NULL AUTO_INCREMENT;
