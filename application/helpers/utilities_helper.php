<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function panjangStr($input, $len=100)	//digunakan untuk memotong string.....
	{
		if (strlen($input)>$len)
			$ret=mb_substr($input,0,$len-4)."....";
		else
			$ret=$input;
		return $ret;
	}

	function tgl_indo($date)
	{
		// format asal 2014-12-25
		$day = date('N',strtotime($date));
		$tgl = date('d-m-Y',strtotime($date));
		$gabungan = kodeHari($day).', '.$tgl;
		return $gabungan;
	}

 	function DateToIndo($date) { // fungsi atau method untuk mengubah tanggal ke format indonesia
	   // variabel BulanIndo merupakan variabel array yang menyimpan nama-nama bulan
		$BulanIndo = array("Januari", "Februari", "Maret",
		   "April", "Mei", "Juni",
		   "Juli", "Agustus", "September",
		   "Oktober", "November", "Desember");
		$tahun = substr($date, 0, 4); // memisahkan format tahun menggunakan substring
		$bulan = substr($date, 5, 2); // memisahkan format bulan menggunakan substring
		$tgl   = substr($date, 8, 2); // memisahkan format tanggal menggunakan substring
		$result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;
		return($result);
	}
	//echo(DateToIndo("2011-08-25")); //Akan menghasilkan 25 Agustus 2011

	function msgbox($jenis='ok',$kata='Not complete')	//digunakan untuk style pesan seperti alert js......
	{
	 	switch ($jenis) {
			 case 'ok':
				 $output= '
				     <div class="alert alert-success">
						<button class="close" data-dismiss="alert" type="button">×</button>
						<h4>Sukses!</h4>
						'.$kata.'
					</div>';
				 break;
			 case 'error':
				 $output= '
				     <div class="alert alert-danger">
						<button class="close" data-dismiss="alert" type="button">×</button>
						<h4>Gagal!</h4>
						'.$kata.'
					</div>';
				 break;
			case 'warning':
				 $output= '
				     <div class="alert alert-warning">
						<button class="close" data-dismiss="alert" type="button">×</button>
						<h4>Perhatian!</h4>
						'.$kata.'
					</div>';
				 break;
			default:
				$output= '
				     <div class="alert alert-warning">
						<button class="close" data-dismiss="alert" type="button">×</button>
						<h4>Perhatian!</h4>
						'.$kata.'
					</div>';
				 break;
		 }
		return $output;
	}

	function hapus_file($url='')	// digunakan untuk hapus file.....
	{
		$hapus=unlink($url);
		if($hapus) return true;
		else return false;		
	}

	function statusData($angka)
	{
		switch ($angka) {
			case '0':
				$st="Non Aktif";
				break;
			case '1':
				$st="Aktif";
				break;
			case '2':
				$st="Close";
				break;
		}
		return $st;
	}

	function statusYN($angka)
	{
		switch ($angka) {
			case '0':
				$st="Tidak";
				break;
			case '1':
				$st="Ya";
				break;
			default:
				$st = "Undefined";
				break;
		}
		return $st;
	}

	function statusComment($angka)
	{
		switch ($angka) {
			case '0':
				$st="Input";
				break;
			case '1':
				$st="Approved";
				break;
			case '2':
				$st="UnApproved";
				break;
		 
				

			default:
				$st = "Undefined";
				break;
		}
		return $st;
	}

	function formatAngka($angka = 0, $rp = FALSE)
	{
		$str = "";
		$Angka = number_format($angka, 2, ',', '.');
		if($rp == TRUE) $str = 'Rp ' . $Angka;
		else $str = $Angka;
		return $str;
	}

	function anti_injection($dat){
	  	$filter = mysql_real_escape_string(stripslashes(strip_tags(htmlspecialchars($dat,ENT_QUOTES))));
	  	return $filter;
	}
