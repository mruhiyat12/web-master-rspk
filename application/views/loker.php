<?php
$this->load->view('main/_header');
// content
// print_r($data);
?>
	<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
			<?php 
				if($data){
				?>
				<article>
						<div class="post-image">
							<div class="post-heading">
								<h3><a href="#"><?=$data[0]->title?></a></h3>
							</div>
							<img src="<?=base_url('assets/image/article/'.$data[0]->img)?>" alt="" class="img-responsive" />
						</div>
						<p>
							 <?=$data[0]->content?>
						</p>
						<div class="bottom-article">
							<ul class="meta-post">
								<li><i class="fa fa-calendar"></i><a href="#"> Mar 27, 2014</a></li>
								<li><i class="fa fa-user"></i><a href="#"> Admin</a></li>
								<li><i class="fa fa-folder-open"></i><a href="#"> Blog</a></li>
								<li><i class="fa fa-comments"></i><a href="#">4 Comments</a></li>
							</ul>
							<a href="#" class="readmore pull-right">Continue reading <i class="fa fa-angle-right"></i></a>
						</div>
				</article>
				<?php
					}
				?>
				

				<div id="pagination">
					<span class="all">Page 1 of 3</span>
					<span class="current">1</span>
					<a href="#" class="inactive">2</a>
					<a href="#" class="inactive">3</a>
				</div>
				<div class="clear"></div>
			</div>
			<div class="col-md-4">
				<aside class="right-sidebar">
				<div class="widget">
					<form role="form">
					  <div class="form-group">
						<input type="text" class="form-control" id="s" placeholder="Search..">
					  </div>
					</form>
				</div>
				<div class="widget">
					<h5 class="widgetheading">Latest posts</h5>
					<?php
						if($data){
							$total = count($data);	
							$j = 0;						
							foreach ($data as $key => $value) {
								if($j==3)
									break;
							?>
							<ul class="recent">
								<li>
									<img src="<?=base_url("assets/image/article/thumb/".$value->img)?>" style=" width:40px; height:40px;" class="pull-left" alt="" />
									<h6><a href="#"><?=$value->title?></a></h6>
									<p>
										 <?=substr($value->content,1,30).'...'?>
									</p>
								</li>
								</li>
							</ul>

							<?php
							$j++;
							}
						}
					?>
					
				</div>
				
				</aside>
			</div>
			
			
		</div>
	</div>
	</section>
<?php
$this->load->view('main/_footer');

?>