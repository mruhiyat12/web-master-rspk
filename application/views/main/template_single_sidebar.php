<title><?php echo (isset($_TITLE))? $_TITLE : '';?> </title>
    <?php $this->load->view('main/_header');?>
    <section style="margin-top: 80px;margin-bottom: 20px;">
        <div class="container">
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url()?>" >Home</a></li>
                    <li><a href="<?php echo site_url($_PAGE_LINK)?>"  ><?php echo (isset($_PAGE_TITLE))? $_PAGE_TITLE : '';?></a></li>
                    <li><a href="#" ><?php echo (isset($_PAGE_TITLE1))? $_PAGE_TITLE1 : '';?></a></li>
                </ul> 
                <div>
                    <div class="col-lg-9 panel panel-info" align="justify">
                        <div class="panel-body">
                            <?php echo (isset($_PAGE_CONTENT))? $_PAGE_CONTENT : '';?>
                            
                        </div>
                        <hr>
                            <a href="http://www.facebook.com/share.php?u=<?php echo site_url($_PAGE_LINK1);?>"><i class="fa fa-facebook-square fa-3x color-facebook"></i></a>
                            <a href="http://twitter.com/share?url=<?php echo site_url($_PAGE_LINK1);?>"><i class="fa fa-twitter-square fa-3x color-twitter"></i></a>
                            <a href="https://plus.google.com/share?url=<?php echo site_url($_PAGE_LINK1);?>"><i class="fa fa-google-plus-square fa-3x color-google-plus"></i></a>
                            <a href="http://www.linkedin.com/shareArticle?mini=<?php echo site_url($_PAGE_LINK1);?>"><i class="fa fa-linkedin-square fa-3x color-linkedin"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    <?php $this->load->view('main/_right_side');?>
                </div>
            </div>
        </section>
        <?php $this->load->view('main/_footer');?>

