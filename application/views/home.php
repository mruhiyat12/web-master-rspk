<?php
$this->load->view('main/_header');
?>

<section id="featured" class="bg">
    <!-- start slider -->


    <!-- start slider -->
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <!-- Slider -->
                <div id="main-slider" class="main-slider flexslider">
                    <ul class="slides">
                        <?php
                        $slide = $this->model->getListByQuery("SELECT * FROM slide WHERE is_active = 1");
                        if ($slide) {
                            foreach ($slide as $row) {
                        ?>
                                <li>
                                    <img src="<?php echo base_url('assets/image/slide/' . $row->img) ?>" style="height:200;width:600;" alt="" />
                            <?php
                            }
                        }
                            ?>

                                </li>
                    </ul>
                </div>
                <!-- end slider -->
            </div>
        </div>
    </div>
</section>
<section class="callaction">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="cta-text">
                    <h2>Profil Rumah Sakit Permata</h2>
                    <?php
                    $profile = $this->model->getRecord(array('table' => 'page', 'where' => array('is_publish' => 1, 'link' => 'profile')));
                    if ($profile) {
                        echo '

                            <h3>' . substr(strip_tags($profile->content), 0, 142) . '</a></h3>

                        </div>
                    </div>';
                    }
                    ?>

                    <div class="col-lg-4">
                        <div class="cta-btn">
                            <a class="btn btn-theme btn-lg" href="<?= base_url('page/profile'); ?>">Read More <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
</section>


<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <div class="heading text-center">
                    <h2>FASILITAS DAN LAYANAN </h2>
                    <h3>Rumah Sakit Permata Keluarga</h3>
                </div>
            </div>
            <?php
            $artikels = $this->model->getListByQuery("SELECT * FROM v_article WHERE is_publish = 1 AND article_category_id = 3 ORDER BY created_datetime DESC LIMIT 4");
            if ($artikels) {
                foreach ($artikels as $artikel) {
                    /*echo '
                <div class="col-md-3 col-md-3">
                    <div class="thumbnail panel panel-info" style="height: 230px">
                        <a href="'.site_url('article/'.$artikel->link).'" ><img class="img-responsive center-block" src="'.base_url("assets/image/article/thumb/" . $artikel->img ).'" style ="height: 150px" alt=" NO IMAGE">
                            <h5>'. substr(strip_tags($artikel->title), 0,33) .'</span></a></span></h5></a>
                            <span><p>'. substr(strip_tags($artikel->content), 0,100) .'</p></span>
                        </div>
                    </div>';
                }*/

            ?>
                    <div class="col-sm-6 col-md-6">
                        <div class="thumbnail">
                            <img src="<?php echo base_url('assets/image/article/thumb/' . $artikel->img) ?>">
                            <div class="caption">
                                <h3><?php echo $artikel->title ?></h3>
                                <p><?php echo substr(strip_tags($artikel->content), 0, 100); ?></p>
                                <p><a href="<?php echo $artikel->link ?>" class="btn btn-primary" role="button">Lihat</a></p>
                            </div>
                        </div>
                    </div>
            <?php }
            }
            ?>

        </div>
        <div class="col-md-12 col-sm-6 col-lg-12"><a type="button" class="btn btn-info btn-flat" href="<?php echo site_url('article'); ?>">More Fasilitas</a>
        </div>

    </div>

    </div>

    </div>
</section>
<!-- End testimonial -->
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading text-center">
                    <h2>Profile Dokter</h2>
                </div>
                <?php
                $artikels = $this->model->getListByQuery("SELECT * FROM v_article WHERE is_publish = 1 AND article_category_id = 3 ORDER BY created_datetime DESC LIMIT 4");
                if ($artikels) {
                    foreach ($artikels as $artikel) {
                        /*echo '
                <div class="col-md-3 col-md-3">
                    <div class="thumbnail panel panel-info" style="height: 230px">
                        <a href="'.site_url('article/'.$artikel->link).'" ><img class="img-responsive center-block" src="'.base_url("assets/image/article/thumb/" . $artikel->img ).'" style ="height: 150px" alt=" NO IMAGE">
                            <h5>'. substr(strip_tags($artikel->title), 0,33) .'</span></a></span></h5></a>
                            <span><p>'. substr(strip_tags($artikel->content), 0,100) .'</p></span>
                        </div>
                    </div>';
                }*/

                ?>
                        <div class="col-sm-6 col-md-6">
                            <div class="thumbnail">
                                <img src="<?php echo base_url('assets/image/article/thumb/' . $artikel->img) ?>">
                                <div class="caption">
                                    <h3><?php echo $artikel->title ?></h3>
                                    <p><?php echo substr(strip_tags($artikel->content), 0, 100); ?></p>
                                    <p><a href="<?php echo $artikel->link ?>" class="btn btn-primary" role="button">Lihat</a></p>
                                </div>
                            </div>
                        </div>
                <?php }
                }
                ?>

            </div>
            <div class="col-md-12 col-sm-6 col-lg-12"><a type="button" class="btn btn-info btn-flat" href="<?php echo site_url('article'); ?>">More Dokter</a>
            </div>
        </div>
    </div>

    </div>

    </div>
</section>
<!-- divider -->

<!-- end divider -->

<!-- Portfolio Projects -->
<section id="content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading text-center">
                    <h2>Portofolio</h2>
                </div>
                <?php
                /*panggil album*/
                /*$fotos = $this->model->getListByQuery("SELECT * FROM v_gallery WHERE is_publish = 1 ORDER BY created_datetime DESC LIMIT 2");*/
                $fotos = $this->model->getListByQuery("SELECT * FROM v_gallery_header WHERE is_publish = 1 ORDER BY created_datetime DESC LIMIT 4");
                if ($fotos) {
                    foreach ($fotos as $foto) {
                        echo '
                        <div class="col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img class="img-responsive center-block" src="' . base_url("assets/image/gallery/" . $foto->cover) . '" style ="height: 210px" alt="XXX"></a>
                                <a href="' . site_url('album/' . $foto->link) . '"><h4>' . $foto->gallery_name . '</h4></a>
                            </div>
                        </div>';
                    }
                }
                ?>
            </div>
            <div class="col-md-12 col-sm-6 col-lg-12"><a type="button" class="btn btn-info btn-flat" href="<?php echo site_url('album'); ?>">More Foto Album</a>
            </div>
        </div>
    </div>
    </div>



    <section id="content">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h2>Gallery Video</h2>
                    </div>
                    <?php
                    $videos = $this->model->getListByQuery("SELECT * FROM article WHERE is_publish = 1 AND article_category_id = 4 ORDER BY created_datetime DESC LIMIT 1");
                    if ($videos) {
                        foreach ($videos as $artikel) {
                            echo ' 
                        <div id="promotion" class="col-md-6 col-sm-6 " >

                            <div class="ifarem embed-responsive embed-responsive-4by3">
                                <iframe class="embed-responsive-item" width="520" height="320"
                                src="' . $artikel->content . '">
                            </iframe>
                        </div>
                    </div>
                </div>';
                        }
                    }
                    ?>
                </div>
                <div class="col-md-12 col-sm-6 col-lg-12"><a type="button" class="btn btn-info btn-flat" href="<?php echo site_url('video'); ?>">More video</a>
                </div>
            </div>
        </div>

        </div>

        <!-- divider -->

        <!-- end divider -->
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading text-center">
                        <h2>Perusahaan dan Asuransi yang bekerja sama</h2>
                    </div>

                    <!-- clients -->
                    <div class="container">
                        <div class="client-slider">
                            <div class="row">
                                <div class="col-xs-6 col-md-2 aligncenter client">
                                    <img alt="logo" src="<?php echo base_url(); ?>assets/template-public/asset/img/clients/logo1.png" class="img-responsive" />

                                </div>

                                <div class="col-xs-6 col-md-2 aligncenter client">
                                    <img alt="logo" src="<?php echo base_url(); ?>assets/template-public/asset/img/clients/logo2.png" class="img-responsive" />

                                </div>

                                <div class="col-xs-6 col-md-2 aligncenter client">
                                    <img alt="logo" src="<?php echo base_url(); ?>assets/template-public/asset/img/clients/logo3.png" class="img-responsive" />

                                </div>

                                <div class="col-xs-6 col-md-2 aligncenter client">
                                    <img alt="logo" src="<?php echo base_url(); ?>assets/template-public/asset/img/clients/logo4.png" class="img-responsive" />

                                </div>


                                <div class="col-xs-6 col-md-2 aligncenter client">
                                    <img alt="logo" src="<?php echo base_url(); ?>assets/template-public/asset/img/clients/logo5.png" class="img-responsive" />

                                </div>
                                <div class="col-xs-6 col-md-2 aligncenter client">
                                    <img alt="logo" src="<?php echo base_url(); ?>assets/template-public/asset/img/clients/logo6.png" class="img-responsive" />
                                </div>

                            </div>
                        </div>

    </section>


    <!-- /.container -->
    <?php $this->load->view('main/_footer'); ?>