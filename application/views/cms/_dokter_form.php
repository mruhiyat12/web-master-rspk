<?php
$nama_dokter = null;
$bagian = null;
$ket = null;
$alamat = null;
if ($param != null) {
    $dokter = $this->model->getRecord(array(
        'table' => 'dokter', 'where' => array('id_dokter' => $param)
        ));
    if ($dokter) {
        $nama_dokter  = $dokter->nama_dokter;
        $bagian       = $dokter->bagian;
        $ket          = $dokter->ket;
        $alamat       = $dokter->alamat;
    }
}
?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs pull-right">
        <li class="active">
            <a data-toggle="tab" href="#dokter-table-tab">
                <i class="fa fa-table"></i>
            </a>
        </li>
        <li class="">
            <a data-toggle="tab" href="#dokter-form-tab">
                <i class="fa fa-edit"></i>
            </a>
        </li>
         <li class="">
            <a data-toggle="tab" href="#dokter-detail-tab" title="Detail View">
                <i class="fa fa-eye"></i>
            </a>
        </li>
        <li class="pull-left header"><i class="fa fa-user-md"></i>Data Dokter</li>
        <div id="loading"></div>
    </ul>
    <div class="tab-content">
        <div id="dokter-table-tab" class="tab-pane fade active in">
            <table id="table-dokter" class="table table-hover table-condensed">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Dokter</th>
                        <th>Bagian</th>
                        <th>Rumah Sakit</th>
                        <th><a href="#" class="btn btn-xs btn-success" onclick="newForm()" id="btn-add"> <i class="fa fa-plus"></i> Add Data</a></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

        <div id="dokter-detail-tab" class="tab-pane fade" >
   
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Sheena Shrestha</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="http://babyinfoforyou.com/wp-content/uploads/2014/10/avatar-300x300.png" class="img-circle img-responsive"> </div>
                
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Department:</td>
                        <td>Programming</td>
                      </tr>
                      <tr>
                        <td>Hire date:</td>
                        <td>06/23/2013</td>
                      </tr>
                      <tr>
                        <td>Date of Birth</td>
                        <td>01/24/1988</td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td>Gender</td>
                        <td>Female</td>
                      </tr>
                        <tr>
                        <td>Home Address</td>
                        <td>Kathmandu,Nepal</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:info@support.com">info@support.com</a></td>
                      </tr>
                        <td>Phone Number</td>
                        <td>123-4567-890(Landline)<br><br>555-4567-890(Mobile)
                        </td>
                           
                      </tr>
                     
                    </tbody>
                  </table>
                  
                  <!-- <a href="#" class="btn btn-primary">My Sales Performance</a>
                  <a href="#" class="btn btn-primary">Team Sales Performance</a> -->
                </div>
              </div>
            </div>
                <div class="panel-footer">
                    <a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-envelope"></i></a>
                    <span class="pull-right">
                        <a href="edit.html" data-original-title="Edit this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-edit"></i></a>
                        <a data-original-title="Remove this user" data-toggle="tooltip" type="button" class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                    </span>
                </div>
            
          </div>
        </div>

        <div id="dokter-form-tab" class="tab-pane fade">
            <form class="form-horizontal role="form" id="dokter-form">
                <div class="form-group">
                    <label for="nama_dokter-input" class="col-md-3 control-label">Nama Dokter</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="nama_dokter-input" name="nama_dokter-input" placeholder="Nama Dokter"/>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="bagian-input" class="col-md-3 control-label">Bagian</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="bagian-input" name="bagian-input" placeholder="Nama Bagian"/>
                    </div>
                </div>
                 <div class="form-group">
                    <label for="dokter-input" class="col-md-3 control-label">Rumah Sakit</label>
                    <div class="col-md-6">
                        <select class="form-control"  name="rumah_sakit-input" id="rumah_sakit-input">
                            <?php
                            $cat = $this->model->getList(array('table' => 'rumah_sakit', 'where' => array('is_active' => 1),  'sort' => 'nama_rumahsakit ASC'));
                            if ($cat) {
                                foreach ($cat as $row) {
                                    echo '<option value="'. $row->id_rumahsakit .'">'. $row->nama_rumahsakit .'</option>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
               <div class="form-group">
                    <label for="ket-input" class="col-md-3 control-label">Keterangan</label>
                    <div class="col-md-9">
                        <textarea class="form-control" id="ket-input" name="ket-input"><?php echo $ket;?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="alamat-input" class="col-md-3 control-label">Alamat</label>
                    <div class="col-md-9">
                        <textarea class="form-control" id="alamat-input" name="alamat-input"><?php echo $alamat;?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="status-input" class="col-md-3 control-label">Status</label>
                    <div class="col-md-6">
                        <select class="form-control"  name="status-input" id="status-input">
                            <option value="1">Aktif</option>
                            <option value="0">Non Aktif</option>
                        </select>
                    </div>
                </div>
                <div class="form-group" id="div-foto" style="display:none;">
                    <label class="control-label col-md-3" for="file_upload">Image</label>
                    <div class="col-md-6">
                        <div id="foto-div"></div>
                        <input id="file_upload" name="file_upload" class="image" type="file" >
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                        <input type="hidden" id="model-input" name="model-input" value="dokter" >
                        <input type="hidden" id="action-input" name="action-input" value="1" >
                        <input type="hidden" id="key-input" name="key-input" value="id_dokter" >
                        <input type="hidden" id="value-input" name="value-input" value="0" >
                        <button type="button" id="btn-save" class="btn btn-success"  onclick="saving(); return false;"><i class="fa fa-save"></i> Save</button>
                        <button type="reset" class="btn btn-default" onclick="setActiveTab('dokter-table-tab');"><i class="fa fa-undo"></i> Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>    
    <script>
    $(document).ready(function () {
        var editor;
        CKEDITOR.replace('ket-input');
        getDokter();
        <?php
        if($param != null) {
            echo 'getData("'. $param .'");';
            echo 'setActiveTab("dokter-form-tab");';
            echo '$("#div-foto").show();';
        }
        ?>

        editor = new $.fn.dataTable.Editor( {
        ajax: base_url + 'objects/dokter',
        table: "#table-dokter",
        fields: [ {
                label: "Nama Dokter:",
                name: "nama_dokter"
            }, {
                label: "Rumah Sakit:",
                name: "nama_rumahsakit"
            }, {
                label: "Keterangan:",
                name: "ket"
            }, {
                label: "Alamat:",
                name: "alamat"
            }
        ]
    } );
 
    // Activate an inline edit on click of a table cell
    $('#table-dokter').on( 'click', 'tbody td:not(:first-child)', function (e) {
        editor.inline( this );
    } );

         // file upload
        $("#file_upload").fileinput({
            maxFileCount: 1,
            browseClass: "btn btn-default",
            browseLabel: "Pilih file",
            browseIcon: '<i class="fa fa-file"></i> ',
            removeClass: "btn btn-warning",
            removeLabel: "Hapus",
            removeIcon: '<i class="glyphicon glyphicon-trash"></i> ',
            uploadClass: "btn btn-info",
            uploadLabel: "Unggah",
            uploadIcon: '<i class="fa fa-cloud-upload"></i> ',
            previewFileType: "image",
            uploadUrl: "<?php echo base_url('doupload'); ?>",
            msgFilesTooMany: 'Jumlah berkas yang akan diunggah ({n}) melebihi batas jumlah yang sudah ditentukan ({m}). Coba ulangi proses unggah berkas!',
            msgLoading: 'Memproses berkas {index} dari {files} …',
            msgProgress: 'Memproses berkas {index} dari {files} - {name} - {percent}% selesai.',
            uploadExtraData: function() {
                return {
                    nama_field:'file_upload',
                    model:'dokter',
                    key: 'id_dokter',
                    value: '<?php echo $param;?>'
                };
            }
        });

        //refresh if succes upload...
        $('#file_upload').on('filebatchuploadcomplete', function(event, files, extra) {
            loadContent(base_url + "view/_dokter_form/<?php echo $param;?>");
        });
    });

     function newForm() {
        loadContent(base_url + "view/_dokter_form", function () {
            setActiveTab("dokter-form-tab");
        });
    }

    function getDokter() {
        if ($.fn.dataTable.isDataTable('#table-dokter')) {
            tableDokter = $('#table-dokter').DataTable();
        } else {
            tableDokter = $('#table-dokter').DataTable({
                "ajax": base_url + 'objects/dokter',
                "columns": [
                   {"data": "DT_RowId", "width": "5%"},
                   {"data": "nama_dokter"},
                   {"data": "bagian"},
                   {"data": "nama_rumahsakit"},
                   {"data": "aksi", "width": "10%"}
               ],
               "columnDefs": [
                    {                        
                        "className" : "dt-body-center", "targets": [ 3 ],
                    }
                ],
                "ordering": true,
                "deferRender": true,
                "order": [[0, "asc"]],
                "fnDrawCallback": function (oSettings) {
                    utilsDokter();
                }
            });
        }
    }

    function utilsDokter() {
        $("#table-dokter .editBtn").on("click",function() {
            loadContent(base_url + 'view/_dokter_form/' + $(this).attr('href').substring(1));
        });

        $("#table-dokter .removeBtn").on("click",function() {
            confirmDelete($(this).attr('href').substring(1));
        });
    }
    function saving() {
        CKupdate();
        loading('loading',true);
        setTimeout(function() {
            $.ajax({
                url: base_url + 'manage',
                data: $("#dokter-form").serialize(),
                dataType: 'json',
                type: 'POST',
                cache: false,
                success: function(json) {
                    loading('loading',false);
                    if (json.data.code === 0) {
                        if (json.data.message == '') {
                            genericAlert('Penyimpanan data gagal!', 'error','Error');
                        } else {
                            genericAlert(json.data.message, 'warning','Peringatan');
                        }
                    } else {
                        var page ='_dokter_form/';
                        page += json.data.last_id;
                        genericAlert('Penyimpanan data berhasil', 'success','Sukses');
                        loadContent(base_url + 'view/' + page);
                    }
                }, error: function () {
                    loading('loading',false);
                    genericAlert('Terjadi kesalahan!', 'error','Error');
                }
            });
        }, 100);
    }

    function getData(idx) {
        $.ajax({
            url: base_url + 'object',
            data: 'model-input=dokter&key-input=id_dokter&value-input=' + idx,
            dataType: 'json',
            type: 'POST',
            cache: false,
            success: function(json) {
                if (json['data'].code === 0) {
                    loginAlert('Akses tidak sah');
                } else {
                    $("#nama_dokter-input").val(json.data.object.nama_dokter);
                    $("#bagian-input").val(json.data.object.bagian);
                    $("#rumah_sakit-input").val(json.data.object.id_rumahsakit);
                    $("#ket-input").val(json.data.object.ket);
                    $("#alamat-input").val(json.data.object.alamat);
                    $("#status-input").val(json.data.object.is_active);
                    $("#action-input").val('2');
                    $("#value-input").val(json.data.object.id_dokter);
                    if (json.data.object.img !== null) {
                        $("#foto-div").html('<img src="<?php echo base_url();?>assets/image/dokter/'+json.data.object.img+'" class="img img-thumbnail img-small">');
                    }
                 
                }
            }
        });
    }

    function confirmDelete(n){
        swal({
            title: "Konfirmasi Hapus",
            text: "Apakah anda yakin akan menghapus data ini?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: " Ya",
            closeOnConfirm: false
        },
        function(){
            loading('loading',true);
            setTimeout(function() {
                $.ajax({
                    url: base_url + 'manage',
                    data: 'model-input=dokter&action-input=3&key-input=id_dokter&value-input='+n,
                    dataType: 'json',
                    type: 'POST',
                    cache: false,
                    success: function(json){
                        loading('loading',false);
                        if (json['data'].code === 1) {
                            genericAlert('Hapus data berhasil','success','Sukses');
                            refreshTable();
                            setActiveTab('dokter-table-tab');
                        } else if(json['data'].code === 2){
                            genericAlert('Hapus data gagal!','error','Error');
                        } else{
                            genericAlert(json['data'].message,'warning','Perhatian');
                        }
                    },
                    error: function () {
                        loading('loading',false);
                        genericAlert('Tidak dapat hapus data!','error', 'Error');
                    }
                });
            }, 100);
        });
    }

    function refreshTable(){
        tableDokter.ajax.url(base_url + '/objects/dokter').load();
    }
    function CKupdate(){
        for ( instance in CKEDITOR.instances )
            CKEDITOR.instances[instance].updateElement();
    }

</script>