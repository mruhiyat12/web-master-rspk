<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DokterModel extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'dokter';
        $this->imgFolder = 'dokter';
        $this->isNew = false;
    }

    public function getField($inputs = array()) {
        $fields = array(
            'nama_dokter'     => $inputs['nama_dokter-input'],
            'bagian'          => $inputs['bagian-input'],
            'id_rumahsakit'   => (isset($inputs['rumah_sakit-input'])) ? $inputs['rumah_sakit-input'] : null,
            'alamat'          => $inputs['alamat-input'],
            'ket'             => $inputs['ket-input'],
           'is_active'        => $inputs['status-input']
           
        );


        return $fields;
    }

    public function getRules() {
        $newRule = ($this->isNew) ? '|is_unique[' . $this->table . '.nama_dokter]' : '';
        $nama_dokter = array(
            'field' => 'nama_dokter-input',
            'label' => 'Title ',
            'rules' => 'trim|required|max_length[255]' . $newRule
        );

        $bagian = array(
            'field' => 'bagian-input',
            'label' => 'Content',
            'rules' => 'trim|required|max_length[255]'
        );
        
        return array($nama_dokter, $bagian);
    }
}