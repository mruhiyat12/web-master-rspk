<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JadwalDokterModel extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'jadwal_dokter';
        $this->imgFolder = 'jadwal_dokter';
        $this->isNew = false;
    }

    public function getField($inputs = array()) {
        $fields = array(
            'id_dokter'     => $inputs['id_dokter-input'],
            'hari'          => $inputs['hari-input'],
            'jam'          => $inputs['jam-input'],
            'id_rumahsakit'   => $inputs['id_rumahsakit-input'],
            'spesialis'          => $inputs['spesialis-input'],
            'ket'             => $inputs['ket-input'],
            'is_active'        => $inputs['status-input']
            
            );


        return $fields;
    }

    public function getRules() {
        $newRule = ($this->isNew) ? '|is_unique[' . $this->table . '.id_dokter]' : '';
        $dokter = array(
            'field' => 'id_dokter-input',
            'label' => 'Dokter ',
            'rules' => 'trim|required|max_length[255]' . $newRule
            );        
        return array($dokter);
    }
}